package com.xstudio.pvzreborn.gui.container.dataslot;

import com.xstudio.pvzreborn.capability.Sunshine;
import net.minecraft.world.inventory.DataSlot;
import net.minecraftforge.common.util.LazyOptional;

public class SunshineDataSlot extends DataSlot {

    private final LazyOptional<Sunshine> sunshine;

    public SunshineDataSlot(LazyOptional<Sunshine> sunshine) {
        this.sunshine = sunshine;
    }

    @Override
    public int get() {
        return sunshine.isPresent() ? sunshine.resolve().get().getValue() : 0;
    }

    @Override
    public void set(int pValue) {
        if (sunshine.isPresent()) {
            sunshine.resolve().get().setValue(pValue);
        }
    }

}
