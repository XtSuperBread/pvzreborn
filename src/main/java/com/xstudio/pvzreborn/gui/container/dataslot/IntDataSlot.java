package com.xstudio.pvzreborn.gui.container.dataslot;

import net.minecraft.world.inventory.DataSlot;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class IntDataSlot extends DataSlot {

    private final Supplier<Integer> getter;
    private final Consumer<Integer> setter;

    public IntDataSlot(Supplier<Integer> getter, Consumer<Integer> setter) {
        this.getter = getter;
        this.setter = setter;
    }

    @Override
    public int get() {
        return getter.get();
    }

    @Override
    public void set(int pValue) {
        setter.accept(pValue);
    }

}
