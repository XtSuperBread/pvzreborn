package com.xstudio.pvzreborn.gui.container;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PVZMenuProvider<T extends AbstractContainerMenu> implements MenuProvider {

    private final Factory<T> factory;
    private final TranslatableComponent transKey;

    public PVZMenuProvider(String transKey, Factory<T> factory) {
        this.factory = factory;
        this.transKey = new TranslatableComponent(transKey);
    }

    @Override
    public @NotNull Component getDisplayName() {
        return transKey;
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pPlayerInventory, Player pPlayer) {
        return factory.create(pContainerId, pPlayerInventory);
    }

    public interface Factory<T extends AbstractContainerMenu> {
        T create(int containerId, Inventory playerInventory);
    }

}
