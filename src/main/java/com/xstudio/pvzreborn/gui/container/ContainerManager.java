package com.xstudio.pvzreborn.gui.container;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.block.BlockNames;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ContainerManager {

	private static final Map<String, RegistryObject<MenuType<?>>> MENU_TYPES = new HashMap<>();
	private static final DeferredRegister<MenuType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, PVZReborn.MOD_ID);

	public static void init(IEventBus bus) {
        PVZReborn.LOGGER.info("Initializing containers");
        CONTAINERS.register(bus);
        registerMenuType(BlockNames.SEED_PACKET_CREATOR, ContainerSeedPacketCreator::new);
        registerMenuType(BlockNames.POLAR_CRAFTING_TABLE, ContainerPolarCraftingTable::new);
        PVZReborn.LOGGER.info("Initialized containers");
    }

    public static <T extends AbstractContainerMenu> void registerMenuType(String name, IContainerFactory<T> factory) {
        RegistryObject<MenuType<?>> menuType = CONTAINERS.register(name, () -> IForgeMenuType.create(factory));
        MENU_TYPES.put(name, menuType);
    }

    @SuppressWarnings("unchecked")
    public static <T extends AbstractContainerMenu> MenuType<T> getMenuType(String name) {
        return (MenuType<T>) Objects.requireNonNull(MENU_TYPES.get(name), "无法获取MenuType:" + name).get();
    }

}
