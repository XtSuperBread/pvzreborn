package com.xstudio.pvzreborn.gui.hud;

import com.mojang.blaze3d.systems.RenderSystem;
import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.utils.CapabilityUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.client.gui.IIngameOverlay;

import java.awt.*;

public class ManagerOverlay {
	static int color = new Color(0xFF0FFFFF, true).getRGB();
	public static final IIngameOverlay PVZ_BASE_HUD = (gui, poseStack, partialTicks, width, height) -> {
		int y = height - 52;
		Font font = gui.getFont();
		Player player = Minecraft.getInstance().player;
		RenderSystem.setShaderTexture(0, PVZReborn.prefix("textures/gui/hud.png"));
		GuiComponent.blit(poseStack, 0, y, gui.getBlitOffset(), 0, 0, 16, 52, 64, 64);
		CapabilityUtils.getDaveCoin(player).ifPresent(daveCoin -> font.draw(poseStack, ": " + daveCoin.getValue(), 18, y + 4, color));
		CapabilityUtils.getSunshine(player).ifPresent(energy -> font.draw(poseStack, ": " + energy.getValue() + " / " + energy.getMaxValue(), 18, y + 21, color));
		CapabilityUtils.getPlantFood(player).ifPresent(plantFood -> font.draw(poseStack, ": " + plantFood.getValue() + " / " + plantFood.getMaxValue(), 18, y + 40, color));
	};

}
