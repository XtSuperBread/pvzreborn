package com.xstudio.pvzreborn.gui.element;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.xstudio.pvzreborn.gui.screen.PVZMachineScreenBase;
import com.xstudio.pvzreborn.gui.screen.PVZScreenBase;
import com.xstudio.pvzreborn.utils.MathUtils;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.TextComponent;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public class ProcessElement extends ScreenElement {

    private final Supplier<Float> processSupplier;
    private float process;

    public ProcessElement(int x, int y, Supplier<Float> processSupplier, PVZScreenBase<?> screen, ShouldRender predicate) {
        super(x, y, screen, predicate);
        this.processSupplier = processSupplier;
    }

    @Override
    public void renderBg(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, PVZMachineScreenBase.MACHINE_MISC);
        screen.blit(poseStack, startRenderX, startRenderY, 21, 66, Math.round(20 * process), 20);
        if (mouseX > startRenderX && mouseX < endRenderX && mouseY > startRenderY && mouseY < endRenderY) {
            screen.renderTooltip(poseStack, new TextComponent(MathUtils.exact(process * 100, 2) + "%"), mouseX, mouseY);
        }
    }

    @Override
    public void init(int relX, int relY) {
        super.init(relX, relY);
        endRenderX = startRenderX + 20;
        endRenderY = startRenderY + 20;
        process = processSupplier.get();
    }

    public float getProcess() {
        return process;
    }

}
