package com.xstudio.pvzreborn.gui.element;

import com.mojang.blaze3d.vertex.PoseStack;
import com.xstudio.pvzreborn.gui.screen.PVZScreenBase;
import org.jetbrains.annotations.NotNull;

public abstract class ScreenElement {

    protected final int x;
    protected final int y;
    protected int endRenderX;
    protected int endRenderY;
    protected int startRenderX;
    protected int startRenderY;
    protected final PVZScreenBase<?> screen;
    protected ShouldRender predicate = (poseStack, partialTick, relX, relY, mouseX, mouseY) -> true;

    protected ScreenElement(int x, int y, PVZScreenBase<?> screen, ShouldRender predicate) {
        this.x = x;
        this.y = y;
        this.screen = screen;
        this.predicate = predicate;
    }

    protected ScreenElement(int x, int y, PVZScreenBase<?> screen) {
        this.x = x;
        this.y = y;
        this.screen = screen;
    }

    public final void renderBackground(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY) {
        if (shouldRender(poseStack, partialTick, relX, relY, mouseX, mouseY)) {
            init(relX, relY);
            renderBg(poseStack, partialTick, relX, relY, mouseX, mouseY);
        }
    }

    public boolean shouldRender(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY) {
        return predicate.shouldRender(poseStack, partialTick, relX, relY, mouseX, mouseY);
    }

    public void init(int relX, int relY) {
        startRenderX = relX + x;
        startRenderY = relY + y;
    }

    public abstract void renderBg(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY);

    @FunctionalInterface
    public interface ShouldRender {

        boolean shouldRender(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY);

    }

}
