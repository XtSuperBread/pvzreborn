package com.xstudio.pvzreborn.gui.element;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.xstudio.pvzreborn.capability.Sunshine;
import com.xstudio.pvzreborn.gui.screen.PVZMachineScreenBase;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.TranslatableComponent;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public class SunshineElement extends ScreenElement {

	private final Supplier<Integer> vOffsetSupplier;
	private int vOffset;

	public SunshineElement(int x, int y, Supplier<Integer> vOffset, PVZMachineScreenBase<?> screen) {
		super(x, y, screen);
		this.vOffsetSupplier = vOffset;
		this.predicate = (poseStack, partialTick, relX, relY, mouseX, mouseY) -> getVOffset() != 64;
	}

	@Override
	public void renderBg(@NotNull PoseStack poseStack, float partialTick, int relX, int relY, int mouseX, int mouseY) {
		RenderSystem.setShader(GameRenderer::getPositionTexShader);
		RenderSystem.setShaderTexture(0, PVZMachineScreenBase.MACHINE_MISC);
		screen.blit(poseStack, startRenderX, startRenderY + vOffset, 16, 0, 16, 64 - vOffset);
		if (mouseX > startRenderX && mouseX < endRenderX && mouseY > startRenderY && mouseY < endRenderY) {
			Sunshine sunshineEnergy = ((PVZMachineScreenBase<?>) screen).getEnergy().resolve().get();
			screen.renderTooltip(poseStack, new TranslatableComponent("gui.pvzreborn.sunshine_energy.tooltip.desc", sunshineEnergy.getValue(), sunshineEnergy.getMaxValue()), mouseX, mouseY);
		}
	}

	@Override
	public void init(int relX, int relY) {
		super.init(relX, relY);
		endRenderX = startRenderX + 16;
		endRenderY = startRenderY + 64;
		vOffset = vOffsetSupplier.get();
	}

	public int getVOffset() {
		return vOffset;
	}

}
