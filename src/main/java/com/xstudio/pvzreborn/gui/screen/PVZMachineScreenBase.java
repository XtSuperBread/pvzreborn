package com.xstudio.pvzreborn.gui.screen;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.capability.Sunshine;
import com.xstudio.pvzreborn.gui.container.BaseContainer;
import com.xstudio.pvzreborn.gui.element.ProcessElement;
import com.xstudio.pvzreborn.gui.element.ScreenElement.ShouldRender;
import com.xstudio.pvzreborn.gui.element.SunshineElement;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.util.LazyOptional;

import java.util.function.Supplier;

/**
 * @author Bread_NiceCat
 */
public abstract class PVZMachineScreenBase<T extends BaseContainer<?>> extends PVZScreenBase<T> {

	public static final ResourceLocation MACHINE_MISC = PVZReborn.prefix("textures/gui/machine_misc.png");
	protected final LazyOptional<Sunshine> energy;

	public PVZMachineScreenBase(T pMenu, Inventory pPlayerInventory, Component pTitle, LazyOptional<Sunshine> energy, ResourceLocation gui) {
		super(pMenu, pPlayerInventory, pTitle, gui);
		this.energy = energy;
	}

	public LazyOptional<Sunshine> getEnergy() {
		return energy;
	}

	public ProcessElement newProcessElement(int x, int y, Supplier<Float> processSupplier, ShouldRender predicate) {
		return new ProcessElement(x, y, processSupplier, this, predicate);
	}

	public ProcessElement newProcessElement(int x, int y, Supplier<Float> processSupplier) {
		return newProcessElement(x, y, processSupplier, (poseStack, partialTick, relX, relY, mouseX, mouseY) -> menu.getBlockEntity().isWorking());
	}

	/**
	 * 自动修正贴图偏移问题
	 *
	 * @author Bread_NiceCat
	 */
	public ProcessElement newProcessElementXTVer(int rawX, int rawY, Supplier<Float> processSupplier) {
		return newProcessElement(rawX + 1, rawY + 2, processSupplier);
	}

	public SunshineElement newEnergyElement(int x, int y) {
		return new SunshineElement(x, y, this::getTextureEnergyOffSet, this);
	}

	protected int getTextureEnergyOffSet() {
		if (energy.isPresent()) {
			Sunshine currentEnergy = energy.resolve().get();
			double percent = 1 - (double) currentEnergy.getValue() / currentEnergy.getMaxValue();
			return percent <= 0 ? 0 : (int) Math.floor(64 * percent);
		}
		return 64;
	}

}
