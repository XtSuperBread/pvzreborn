package com.xstudio.pvzreborn.gui.screen;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.xstudio.pvzreborn.gui.element.ScreenElement;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bread_NiceCat
 */
public abstract class PVZScreenBase<T extends AbstractContainerMenu> extends AbstractContainerScreen<T> {
    protected final ResourceLocation gui;
    protected final List<ScreenElement> elements = new ArrayList<>();

    public PVZScreenBase(T pMenu, Inventory pPlayerInventory, Component pTitle, ResourceLocation gui) {
        super(pMenu, pPlayerInventory, pTitle);
        this.gui = gui;
    }

    @Override
    public void render(PoseStack pPoseStack, int mouseX, int mouseY, float pPartialTick) {
        renderBackground(pPoseStack);
        super.render(pPoseStack, mouseX, mouseY, pPartialTick);
        renderTooltip(pPoseStack, mouseX, mouseY);
    }

    @Override
    protected void renderLabels(@NotNull PoseStack pPoseStack, int pMouseX, int pMouseY) {
        drawCenteredString(pPoseStack, font, this.title, 52, 5, 4210752);
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        int relX = (this.width - this.imageWidth) / 2;
        int relY = (this.height - this.imageHeight) / 2;
        RenderSystem.setShaderTexture(0, gui);
        this.blit(pPoseStack, relX, relY, 0, 0, this.imageWidth, this.imageHeight);
        for (ScreenElement element : this.elements) {
            element.renderBackground(pPoseStack, pPartialTick, relX, relY, pMouseX, pMouseY);
        }
    }

    public void addElement(ScreenElement element) {
        this.elements.add(element);
    }

    public void addElement(List<ScreenElement> elements) {
        this.elements.addAll(elements);
    }

}
