package com.xstudio.pvzreborn.gui.screen;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.gui.container.ContainerPolarCraftingTable;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ScreenPolarCraftingTable extends PVZMachineScreenBase<ContainerPolarCraftingTable> {

	public static final ResourceLocation GUI = PVZReborn.prefix("textures/gui/polar_crafting_table_gui.png");

	public ScreenPolarCraftingTable(ContainerPolarCraftingTable pMenu, Inventory pPlayerInventory, Component pTitle) {
		super(pMenu, pPlayerInventory, pTitle, pMenu.getSunshineEnergy(), GUI);

		addElement(newProcessElementXTVer(108, 33, () -> pMenu.getBlockEntity().getProcess()));
		addElement(newEnergyElement(78, 11));
	}

}
