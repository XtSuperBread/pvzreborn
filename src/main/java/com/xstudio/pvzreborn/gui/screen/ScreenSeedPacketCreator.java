package com.xstudio.pvzreborn.gui.screen;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.gui.container.ContainerSeedPacketCreator;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ScreenSeedPacketCreator extends PVZMachineScreenBase<ContainerSeedPacketCreator> {

	public static final ResourceLocation GUI = PVZReborn.prefix("textures/gui/seed_packet_crafting_gui.png");

	public ScreenSeedPacketCreator(ContainerSeedPacketCreator pMenu, Inventory pPlayerInventory, Component pTitle) {
		super(pMenu, pPlayerInventory, pTitle, pMenu.getSunshineEnergy(), GUI);

		addElement(newEnergyElement(148, 10));
		addElement(newProcessElement(79, 17, () -> pMenu.getBlockEntity().getProcess()));
	}

}
