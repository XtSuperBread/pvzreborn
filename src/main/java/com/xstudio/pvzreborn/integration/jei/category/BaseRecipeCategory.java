package com.xstudio.pvzreborn.integration.jei.category;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.block.entity.PVZMachineBlockEntityBase;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.recipe.RecipeBase;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public abstract class BaseRecipeCategory<T extends RecipeBase> implements IRecipeCategory<T> {
	protected static final int MAX_ENERGY = PVZMachineBlockEntityBase.MAX_ENERGY;
	protected final IGuiHelper guiHelper;
	protected final IDrawable background;

	public BaseRecipeCategory(IGuiHelper guiHelper, IDrawable background) {
		this.guiHelper = guiHelper;
		this.background = background;
	}

	@SuppressWarnings("removal")
	@Override
	@NotNull
	public ResourceLocation getUid() {
		return getRecipeType().getUid();
	}

	@SuppressWarnings("removal")
	@Override
	@Nonnull
	public Class<? extends T> getRecipeClass() {
		return getRecipeType().getRecipeClass();
	}

	@NotNull
	@Override
	public IDrawable getBackground() {
		return background;
	}

	@NotNull
	@Override
	public Component getTitle() {
		return new TranslatableComponent("block." + PVZReborn.MOD_ID + "." + getBlockRegistryName());
	}

	@NotNull
	@Override
	public IDrawable getIcon() {
		return guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, ItemManager.getItem(getBlockRegistryName()).getDefaultInstance());
	}

	public abstract String getBlockRegistryName();

}
