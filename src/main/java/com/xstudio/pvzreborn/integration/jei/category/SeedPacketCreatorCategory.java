package com.xstudio.pvzreborn.integration.jei.category;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.gui.screen.ScreenSeedPacketCreator;
import com.xstudio.pvzreborn.integration.jei.PVZRecipeTypes;
import com.xstudio.pvzreborn.recipe.machine.SPCRecipe;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import org.jetbrains.annotations.NotNull;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class SeedPacketCreatorCategory extends BaseMachineRecipeCategory<SPCRecipe> {

	public SeedPacketCreatorCategory(IGuiHelper guiHelper) {
		super(guiHelper, guiHelper.createDrawable(ScreenSeedPacketCreator.GUI, 3, 3, 166, 78), 76, 14, 145, 7, 117, 0);
	}

	@Override
	public String getBlockRegistryName() {
        return BlockNames.SEED_PACKET_CREATOR;
	}

	@NotNull
	@Override
	public RecipeType<SPCRecipe> getRecipeType() {
		return PVZRecipeTypes.SPC;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, SPCRecipe recipe, IFocusGroup focuses) {
		builder.addSlot(RecipeIngredientRole.OUTPUT, 117, 30).addItemStack(recipe.getResultItem());
		builder.addSlot(RecipeIngredientRole.INPUT, 37, 30).addItemStack(Items.PAPER.getDefaultInstance());
		Ingredient[] input = recipe.getInputs();
		for (int i = 0; i < input.length; i++) {
			builder.addSlot(RecipeIngredientRole.INPUT, 3, 3 + 18 * i).addIngredients(input[i]);
		}
	}

}
