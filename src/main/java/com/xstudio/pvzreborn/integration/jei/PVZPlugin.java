package com.xstudio.pvzreborn.integration.jei;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.integration.jei.category.BaseRecipeCategory;
import com.xstudio.pvzreborn.integration.jei.category.PolarCraftingTableCategory;
import com.xstudio.pvzreborn.integration.jei.category.SeedPacketCreatorCategory;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import com.xstudio.pvzreborn.recipe.RecipeBase;
import com.xstudio.pvzreborn.recipe.RecipeManager;
import com.xstudio.pvzreborn.recipe.machine.polarcraftingtable.PCTRecipeBase;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IRecipeManager;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import mezz.jei.api.registration.IRecipeCatalystRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import mezz.jei.api.runtime.IJeiRuntime;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@JeiPlugin
public class PVZPlugin implements IModPlugin {

	private static final Map<ResourceLocation, BaseRecipeCategory<? extends RecipeBase>> RECIPE_CATEGORISE = new HashMap<>();

	@SuppressWarnings("unchecked")
	public static <T> void addRecipes(IRecipeRegistration registration, RecipeType<?> recipeType, List<?> recipes) {
		registration.addRecipes((RecipeType<T>) recipeType, (List<T>) recipes);
	}

	@NotNull
	@Override
	public ResourceLocation getPluginUid() {
        return PVZReborn.prefix("jei_plugin");
	}

	@Override
	public void registerCategories(IRecipeCategoryRegistration registration) {
		IGuiHelper guiHelper = registration.getJeiHelpers().getGuiHelper();
		registerCategory(new SeedPacketCreatorCategory(guiHelper));
		registerCategory(new PolarCraftingTableCategory(guiHelper));
		RECIPE_CATEGORISE.values().forEach(registration::addRecipeCategories);
	}

	@Override
	public void registerRecipes(@NotNull IRecipeRegistration registration) {
		ItemManager.getJEIDescs().forEach((item, info) -> {
            if (I18n.exists(info)) {
                registration.addIngredientInfo(item.get().getDefaultInstance(), VanillaTypes.ITEM_STACK, new TranslatableComponent(info));
            }
        });
		Level level = Minecraft.getInstance().level;
		RECIPE_CATEGORISE.forEach((uid, category) -> addRecipes(registration, category.getRecipeType(), RecipeManager.getRecipes(level, RecipeManager.getRecipeType(uid.getPath()))));
	}

	@Override
	public void registerRecipeCatalysts(@NotNull IRecipeCatalystRegistration registration) {
		RECIPE_CATEGORISE.forEach((uid, category) -> registration.addRecipeCatalyst(ItemManager.getDefaultItemStack(uid.getPath()), category.getRecipeType()));
	}

	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime) {
		IRecipeManager recipeManager = jeiRuntime.getRecipeManager();
		jeiRuntime.getIngredientManager().removeIngredientsAtRuntime(VanillaTypes.ITEM_STACK, Set.of(ItemManager.getDefaultItemStack(ItemNames.KASUALIX_POTATO)));
		recipeManager.hideRecipes(PVZRecipeTypes.PCT, RecipeManager.getRecipesByOutput(Minecraft.getInstance().level, RecipeManager.getRecipeType(PCTRecipeBase.TYPE_ID), ItemManager.getDefaultItemStack(ItemNames.KASUALIX_POTATO), false));
	}

	public <T extends RecipeBase> void registerCategory(BaseRecipeCategory<T> category) {
		RECIPE_CATEGORISE.put(category.getUid(), category);
	}

	@SuppressWarnings("unchecked")
	public <T> IRecipeCategory<T> getCategory(ResourceLocation uid) {
		return (IRecipeCategory<T>) RECIPE_CATEGORISE.get(uid);
	}

}
