package com.xstudio.pvzreborn.integration.jei.category;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.gui.screen.ScreenPolarCraftingTable;
import com.xstudio.pvzreborn.integration.jei.PVZRecipeTypes;
import com.xstudio.pvzreborn.recipe.machine.polarcraftingtable.PCTRecipeBase;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.world.item.crafting.Ingredient;
import org.apache.commons.lang3.mutable.MutableInt;
import org.jetbrains.annotations.NotNull;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class PolarCraftingTableCategory extends BaseMachineRecipeCategory<PCTRecipeBase> {

	public PolarCraftingTableCategory(IGuiHelper guiHelper) {
		super(guiHelper, guiHelper.createDrawable(ScreenPolarCraftingTable.GUI, 3, 3, 166, 78), 106, 32, 75, 8, 132, 0);
	}

	@Override
	public String getBlockRegistryName() {
        return BlockNames.POLAR_CRAFTING_TABLE;
	}

	@NotNull
	@Override
	public RecipeType<PCTRecipeBase> getRecipeType() {
		return PVZRecipeTypes.PCT;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, PCTRecipeBase recipe, IFocusGroup focuses) {
		builder.addSlot(RecipeIngredientRole.OUTPUT, 136, 32).addItemStack(recipe.getResultItem());
		MutableInt index = new MutableInt();
		for (Ingredient input : recipe.getInputs()) {
			int x = 20 + 18 * (index.intValue() % 3);
			int y = 14 + 18 * (int) Math.floor(index.intValue() / 3.0D);
			builder.addSlot(RecipeIngredientRole.INPUT, x, y).addIngredients(input);
			index.increment();
		}
	}

}
