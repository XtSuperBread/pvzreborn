package com.xstudio.pvzreborn.integration.jei.category;

import com.mojang.blaze3d.vertex.PoseStack;
import com.xstudio.pvzreborn.gui.screen.PVZMachineScreenBase;
import com.xstudio.pvzreborn.recipe.machine.MachineRecipeBase;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bread_NiceCat
 */
public abstract class BaseMachineRecipeCategory<T extends MachineRecipeBase> extends BaseRecipeCategory<T> {

	protected final int processorX, processorY, energyX, energyY, shapelessIconX, shapelessIconY;
	private final IDrawableAnimated processor;
	private final IDrawableStatic energy;
	private final IDrawableStatic shapeless;

	public BaseMachineRecipeCategory(IGuiHelper guiHelper, IDrawable background, int processorX, int processorY, int energyX, int energyY, int shapelessIconX, int shapelessIconY) {
		super(guiHelper, background);
		this.processorX = processorX;
		this.processorY = processorY;
		this.energyX = energyX;
		this.energyY = energyY;
		this.shapelessIconX = shapelessIconX;
		this.shapelessIconY = shapelessIconY;
		processor = guiHelper.drawableBuilder(PVZMachineScreenBase.MACHINE_MISC, 21, 66, 20, 20).buildAnimated(100, IDrawableAnimated.StartDirection.LEFT, false);
		energy = guiHelper.drawableBuilder(PVZMachineScreenBase.MACHINE_MISC, 16, 0, 16, 64).build();
		shapeless = guiHelper.drawableBuilder(PVZMachineScreenBase.MACHINE_MISC, 0, 82, 18, 17).build();
	}

	@Override
	public void draw(@NotNull T recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
		processor.draw(stack, processorX, processorY);
		energy.draw(stack, energyX, energyY, Math.round(64 * (1 - (float) recipe.getRequireEnergy() / MAX_ENERGY)), 0, 0, 0);
		if (recipe.isShapeless(recipe)) {
			shapeless.draw(stack, shapelessIconX, shapelessIconY);
		}
	}

	@Override
	public @NotNull List<Component> getTooltipStrings(@NotNull T recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
		List<Component> tooltip = new ArrayList<>(2);
		if (mouseX > processorX && mouseX < processorX + 20 && mouseY > processorY && mouseY < processorY + 20) {
			tooltip.add(new TranslatableComponent("jei.pvzreborn.tick", recipe.getTick() / 20.0D));
		} else if (mouseX > energyX && mouseX < energyX + 16 && mouseY > energyY && mouseY < energyY + 64) {
			tooltip.add(new TranslatableComponent("jei.pvzreborn.require_energy", recipe.getRequireEnergy()));
		}
		return tooltip;
	}
}
