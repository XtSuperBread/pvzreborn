package com.xstudio.pvzreborn.integration.jei;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.recipe.machine.SPCRecipe;
import com.xstudio.pvzreborn.recipe.machine.polarcraftingtable.PCTRecipeBase;
import mezz.jei.api.recipe.RecipeType;

/**
 * @author Bread_NiceCat
 */
public class PVZRecipeTypes {

	public static final RecipeType<SPCRecipe> SPC = new RecipeType<>(PVZReborn.prefix(SPCRecipe.TYPE_ID), SPCRecipe.class);
	public static final RecipeType<PCTRecipeBase> PCT = new RecipeType<>(PVZReborn.prefix(PCTRecipeBase.TYPE_ID), PCTRecipeBase.class);

}
