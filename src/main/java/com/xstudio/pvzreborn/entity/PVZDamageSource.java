package com.xstudio.pvzreborn.entity;

import net.minecraft.world.damagesource.DamageSource;

/**
 * @author Bread_NiceCat
 */
public class PVZDamageSource extends DamageSource {
	public static final DamageSource INVALID_ENVIRONMENT = new PVZDamageSource("plant.invalid_environment");

	/**
	 * @param msgId i18nKey:if !byPlayer ("death.attack." + msgId)
	 *              ,else suffix:".player"(with 2 %s)
	 * @see super.getLocalizedDeathMessage
	 */
	public PVZDamageSource(String msgId) {
		super(msgId);
	}

}
