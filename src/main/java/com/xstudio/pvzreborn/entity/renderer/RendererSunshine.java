package com.xstudio.pvzreborn.entity.renderer;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;
import com.xstudio.pvzreborn.entity.entities.misc.EntitySunshine;
import com.xstudio.pvzreborn.utils.RenderUtils;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 18:10
 */

@OnlyIn(Dist.CLIENT)
public class RendererSunshine extends EntityRenderer<EntitySunshine> {
	private static final ResourceLocation TEXTURE_LOCATION = RenderUtils.entityTexLoc("sunshine");
	private static final RenderType RENDER_TYPE = RenderType.entityCutoutNoCull(TEXTURE_LOCATION);

	public RendererSunshine(EntityRendererProvider.Context pContext) {
		super(pContext);
	}

	private void vertex(VertexConsumer vertexConsumer, Matrix4f pose, Matrix3f normal, int lightmapUV, float x, int y, int u, int v) {
		vertexConsumer.vertex(pose, x - 0.5F, y, 0.0F)
				.color(255, 255, 255, 255)
				.uv(u, v)
				.overlayCoords(OverlayTexture.NO_OVERLAY)
				.uv2(lightmapUV)
				.normal(normal, 0.0F, 1.0F, 0.0F)
				.endVertex();
	}

	protected int getBlockLightLevel(@NotNull EntitySunshine pEntity, @NotNull BlockPos pPos) {
		return 15;
	}

	@Override
	public boolean shouldRender(@NotNull EntitySunshine sunE, @NotNull Frustum pCamera, double pCamX, double pCamY, double pCamZ) {
		if (sunE.getMaxAge() - sunE.tickCount <= EntitySunshine.whenStartFlickering) {
			int tickPeriod = sunE.renderCounter % 4;//40 ticks
			if (tickPeriod < 2) {//0,1不渲染;2,3渲染。即其中前20tick不渲染，后20tick渲染
				return false;
			}
		}
		return super.shouldRender(sunE, pCamera, pCamX, pCamY, pCamZ);
	}

	public void render(@NotNull EntitySunshine sunE, float pEntityYaw, float pPartialTicks, PoseStack pMatrixStack, @NotNull MultiBufferSource pBuffer, int pPackedLight) {
		sunE.renderCounter++;
		pMatrixStack.pushPose();
		int sunshine = sunE.getSunshine();
		if (sunshine <= 25) {
			pMatrixStack.scale(0.5F, 0.5F, 0.5F);
		} else if (sunshine <= 75) {
			pMatrixStack.scale(1F, 1F, 1F);
		} else {
			pMatrixStack.scale(2F, 2F, 2F);
		}
		pMatrixStack.mulPose(this.entityRenderDispatcher.cameraOrientation());
		pMatrixStack.mulPose(Vector3f.YP.rotationDegrees(180.0F));
		PoseStack.Pose pose = pMatrixStack.last();
		Matrix4f matrix4f = pose.pose();
        Matrix3f matrix3f = pose.normal();
        VertexConsumer vertexconsumer = pBuffer.getBuffer(RENDER_TYPE);
        vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 0.0F, 0, 0, 1);
        vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 1.0F, 0, 1, 1);
        vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 1.0F, 1, 1, 0);
        vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 0.0F, 1, 0, 0);
        pMatrixStack.popPose();
        super.render(sunE, pEntityYaw, pPartialTicks, pMatrixStack, pBuffer, pPackedLight);
    }

    @NotNull
    public ResourceLocation getTextureLocation(@NotNull EntitySunshine pEntity) {
        return TEXTURE_LOCATION;
    }

}
