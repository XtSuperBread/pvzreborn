package com.xstudio.pvzreborn.entity.renderer;

import com.xstudio.pvzreborn.entity.entities.plant.EntityPlantWallNut;
import com.xstudio.pvzreborn.entity.models.ModelPlantWallNut;
import com.xstudio.pvzreborn.utils.RenderUtils;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;

@OnlyIn(Dist.CLIENT)
public class RendererPlantWallNut extends LivingEntityRenderer<EntityPlantWallNut, ModelPlantWallNut> {

	public static final ResourceLocation SIMPLE = RenderUtils.getTextureByModelLayerLocation(ModelPlantWallNut.LAYER_LOCATION);

	public RendererPlantWallNut(EntityRendererProvider.Context pContext) {
		super(pContext, new ModelPlantWallNut(pContext.bakeLayer(ModelPlantWallNut.LAYER_LOCATION)), 0.5F);
	}

	@NotNull
	@Override
	public ResourceLocation getTextureLocation(@NotNull EntityPlantWallNut pEntity) {
		return SIMPLE;
	}

}
