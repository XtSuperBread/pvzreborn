package com.xstudio.pvzreborn.entity.entities.plant;

import com.xstudio.pvzreborn.entity.entities.plant.api.IAttractablePlant;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

public class EntityPlantWallNut extends PVZPlantBase implements IAttractablePlant {

	public static final String NAME = "wallnut";

	public EntityPlantWallNut(EntityType<EntityPlantWallNut> pEntityType, Level pLevel) {
		super(pEntityType, pLevel);
	}

	@NotNull
	public static AttributeSupplier.Builder attribute() {
		return createPlantAttributes().add(Attributes.MAX_HEALTH, 4000f);
	}

	@Override
	public AttractLevel attractPriority() {
		return AttractLevel.III;
	}
}
