package com.xstudio.pvzreborn.entity.entities;

import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 16:03
 */
public abstract class PVZEntityBase extends Entity {
	public PVZEntityBase(EntityType<?> pEntityType, Level pLevel) {
		super(pEntityType, pLevel);
	}
	

	@Override
	public @NotNull Packet<?> getAddEntityPacket() {
		return new ClientboundAddEntityPacket(this);
	}
}
