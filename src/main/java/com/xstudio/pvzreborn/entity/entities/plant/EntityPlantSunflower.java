package com.xstudio.pvzreborn.entity.entities.plant;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 20:06
 */
public class EntityPlantSunflower extends PVZPlantBase {
	protected EntityPlantSunflower(EntityType<EntityPlantSunflower> pEntityType, Level pLevel) {
		super(pEntityType, pLevel);
	}
}
