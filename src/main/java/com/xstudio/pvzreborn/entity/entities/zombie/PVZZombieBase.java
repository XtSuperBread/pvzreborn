package com.xstudio.pvzreborn.entity.entities.zombie;

import com.xstudio.pvzreborn.entity.entities.plant.PVZPlantBase;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.ZombieAttackGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.level.Level;

public abstract class PVZZombieBase extends Zombie {

	public PVZZombieBase(EntityType<? extends PVZZombieBase> pEntityType, Level pLevel) {
		super(pEntityType, pLevel);
	}

	protected void addBehaviourGoals() {
		this.goalSelector.addGoal(10, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(2, new ZombieAttackGoal(this, 1.0D, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, PVZPlantBase.class, false));
		this.targetSelector.addGoal(1, (new HurtByTargetGoal(this)).setAlertOthers(PVZZombieBase.class));
	}

}
