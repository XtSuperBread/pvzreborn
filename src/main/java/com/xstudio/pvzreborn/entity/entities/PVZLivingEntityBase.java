package com.xstudio.pvzreborn.entity.entities;

import net.minecraft.core.NonNullList;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

public abstract class PVZLivingEntityBase extends LivingEntity {
	private final NonNullList<ItemStack> armorItems = NonNullList.withSize(4, ItemStack.EMPTY);
	private final NonNullList<ItemStack> handItems = NonNullList.withSize(2, ItemStack.EMPTY);

	protected PVZLivingEntityBase(EntityType<? extends LivingEntity> pEntityType, Level pLevel) {
		super(pEntityType, pLevel);
	}

	@Override
	public @NotNull Iterable<ItemStack> getArmorSlots() {
		return this.armorItems;
	}

	@Override
	public @NotNull ItemStack getItemBySlot(EquipmentSlot pSlot) {
		return switch (pSlot.getType()) {
			case HAND -> this.handItems.get(pSlot.getIndex());
			case ARMOR -> this.armorItems.get(pSlot.getIndex());
		};
	}

	@Override
	public void setItemSlot(EquipmentSlot pSlot, @NotNull ItemStack pStack) {
		this.verifyEquippedItem(pStack);
		switch (pSlot.getType()) {
			case HAND -> this.handItems.set(pSlot.getIndex(), pStack);
			case ARMOR -> this.armorItems.set(pSlot.getIndex(), pStack);
		}
	}

	@Override
	public void tick() {
		super.tick();
	}

	@Override
	public @NotNull HumanoidArm getMainArm() {
		return HumanoidArm.RIGHT;
	}

}
