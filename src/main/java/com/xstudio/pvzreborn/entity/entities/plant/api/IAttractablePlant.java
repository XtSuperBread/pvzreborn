package com.xstudio.pvzreborn.entity.entities.plant.api;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 15:17
 * 僵尸优先吃
 */
public interface IAttractablePlant {
	AttractLevel attractPriority();

	/**
	 * 非攻击性I,攻击性II,坚果III,红薯IV
	 */
	enum AttractLevel {
		O(0), I(1), II(2), III(3), IV(4);

		final int level;

		AttractLevel(int l) {
			this.level = l;
		}
	}

}
