package com.xstudio.pvzreborn.entity.entities.plant.api;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 15:48
 */
public interface IAttackablePlant extends IAttractablePlant {
	@Override
	default AttractLevel attractPriority() {
		return AttractLevel.II;
	}
}
