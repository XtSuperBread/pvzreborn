package com.xstudio.pvzreborn.mixin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.minecraft.data.HashCache;
import net.minecraft.data.recipes.RecipeProvider;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.nio.file.Path;

@Mixin(RecipeProvider.class)
public interface MixinRecipeProvider {

    @Invoker("saveRecipe")
    static void saveRecipe(HashCache cache, JsonObject advancementJson, Path path) {
        throw new IllegalStateException();
    }

    @Accessor("GSON")
    static Gson getGson() {
        throw new IllegalStateException();
    }

}
