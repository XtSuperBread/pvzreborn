package com.xstudio.pvzreborn.mixin;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.core.NonNullList;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.ShapedRecipe;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.Map;

@Mixin(ShapedRecipe.class)
public interface MixinShapedRecipe {

    @Invoker("keyFromJson")
    static Map<String, Ingredient> keyFromJson(JsonObject keyEntry) {
        throw new IllegalStateException();
    }

    @Invoker("shrink")
    static String[] shrink(String... toShrink) {
        throw new IllegalStateException();
    }

    @Invoker("patternFromJson")
    static String[] patternFromJson(JsonArray patternArray) {
        throw new IllegalStateException();
    }

    @Invoker("dissolvePattern")
    static NonNullList<Ingredient> dissolvePattern(String[] pPattern, Map<String, Ingredient> pKeys, int pPatternWidth, int pPatternHeight) {
        throw new IllegalStateException();
    }

}
