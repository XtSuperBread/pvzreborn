package com.xstudio.pvzreborn.mixin;

import com.google.gson.JsonArray;
import net.minecraft.core.NonNullList;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.ShapelessRecipe;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(ShapelessRecipe.Serializer.class)
public interface MixinShapelessRecipe {

    @Invoker("itemsFromJson")
    static NonNullList<Ingredient> itemsFromJson(JsonArray ingredientArray) {
        throw new IllegalStateException();
    }

}
