package com.xstudio.pvzreborn.sound;

import com.xstudio.pvzreborn.PVZReborn;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.HashMap;
import java.util.Map;

public class SoundManager {

	private static final Map<String, RegistryObject<SoundEvent>> SOUND_EVENTS = new HashMap<>();
	private static final DeferredRegister<SoundEvent> SOUND_EVENT_REGISTRY = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, PVZReborn.MOD_ID);

	public static void init(IEventBus bus) {
		PVZReborn.LOGGER.info("Initializing sound");
		SOUND_EVENT_REGISTRY.register(bus);

		registerSoundEvent("soul");
		PVZReborn.LOGGER.info("Initialized sound");
	}

	public static void registerSoundEvent(String name) {
		RegistryObject<SoundEvent> soundEvent = SOUND_EVENT_REGISTRY.register(name, () -> new SoundEvent(PVZReborn.prefix(name)));
		SOUND_EVENTS.put(name, soundEvent);
	}

	public static SoundEvent getSoundEvent(String name) {
		return SOUND_EVENTS.get(name).get();
	}

}
