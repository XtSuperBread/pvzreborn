package com.xstudio.pvzreborn.block.entity;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.handle.PVZItemStackHandler;
import com.xstudio.pvzreborn.recipe.RecipeManager;
import com.xstudio.pvzreborn.recipe.machine.SPCRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class BlockEntitySeedPacketCreator extends PVZMachineBlockEntityBase {

	private final PVZItemStackHandler input = new PVZItemStackHandler(4, onSlotChange);
	private final LazyOptional<IItemHandler> inputCapability = LazyOptional.of(() -> input);
	private final PVZItemStackHandler output = new PVZItemStackHandler(1, onSlotChange);
	private final LazyOptional<IItemHandler> outputCapability = LazyOptional.of(() -> output);
	private final PVZItemStackHandler paper = createPaperItemStackHandler();
	private final LazyOptional<IItemHandler> paperCapability = LazyOptional.of(() -> paper);
	/**
	 * 上一次的配方
	 */
	private SPCRecipe cacheRecipe;

	public BlockEntitySeedPacketCreator(BlockPos worldPosition, BlockState blockState) {
        super(BlockEntityManager.getBlockEntityType(BlockNames.SEED_PACKET_CREATOR), worldPosition, blockState);
	}

	@Override
	public List<IItemHandler> getAllItemStackHandler() {
		return List.of(input, paper, output);
	}

	@Override
	public void setRemoved() {
		super.setRemoved();
		inputCapability.invalidate();
		paperCapability.invalidate();
		outputCapability.invalidate();
	}

	@Override
	public void load(@NotNull CompoundTag pTag) {
		super.load(pTag);
		this.input.deserializeNBT(pTag.getCompound("input"));
		this.paper.deserializeNBT(pTag.getCompound("paper"));
		this.output.deserializeNBT(pTag.getCompound("output"));
	}

	@NotNull
	@Override
	public <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
		if (side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return switch (side) {
				default -> LazyOptional.empty();
				case UP -> paperCapability.cast();
				case DOWN -> outputCapability.cast();
				case SOUTH -> inputCapability.cast();
			};
		}
		return super.getCapability(cap, side);
	}

	@Override
	public void tick() {
		super.tick();
		List<ItemStack> inputs = input.getAllStacks();
		checkWhetherWork(inputs);
		if (isWorking) {
			processTime++;
			if (processTime >= cacheRecipe.getTick() && energy.getValue() >= cacheRecipe.getRequireEnergy()) {
				ItemStack outStack = cacheRecipe.getResultItem().copy();
				energy.remove(cacheRecipe.getRequireEnergy());
				inputs.forEach(stack -> stack.shrink(1));
				paper.getStackInSlot(0).shrink(1);
				output.insertItem(0, outStack, false);
				isWorking = false;
				processTime = 0;
			}
		} else {
			processTime = 0;
		}
	}

	public float getProcess() {
		if (cacheRecipe != null && isWorking) {
			return (float) processTime / cacheRecipe.getTick();
		}
		return 0f;
	}

	public SPCRecipe getRecipe(List<ItemStack> inputs) {
		List<SPCRecipe> allRecipe = RecipeManager.getRecipes(level, RecipeManager.getRecipeType(SPCRecipe.TYPE_ID));
		for (SPCRecipe recipe : allRecipe) {
			if (recipe.matches(inputs, energy.getValue())) {
				this.cacheRecipe = recipe;
				return recipe;
			}
		}
		return null;
	}

	public void checkWhetherWork(List<ItemStack> inputs) {
		if (!paper.getStackInSlot(0).isEmpty()) {
			SPCRecipe recipe = (cacheRecipe != null && cacheRecipe.matches(inputs, energy.getValue())) ? cacheRecipe : getRecipe(inputs);
			if (recipe != null) {
				isWorking = output.insertItem(0, recipe.getResultItem(), true).isEmpty();
			}
		}
	}

	public PVZItemStackHandler getInputHandler() {
		return input;
	}

	@Override
	protected void saveAdditional(CompoundTag pTag) {
		super.saveAdditional(pTag);
		pTag.put("input", input.serializeNBT());
		pTag.put("paper", paper.serializeNBT());
		pTag.put("output", output.serializeNBT());
	}

	private PVZItemStackHandler createPaperItemStackHandler() {
		return new PVZItemStackHandler(onSlotChange) {
			@Override
			public boolean isItemValid(int slot, @NotNull ItemStack stack) {
				return stack.is(Items.PAPER);
			}

			@NotNull
			@Override
			public ItemStack insertItem(int slot, @NotNull ItemStack stack, boolean simulate) {
				if (!stack.is(Items.PAPER)) {
					return stack.copy();
				}
				return super.insertItem(slot, stack, simulate);
			}
		};
	}

}
