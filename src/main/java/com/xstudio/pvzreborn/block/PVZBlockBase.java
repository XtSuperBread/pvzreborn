package com.xstudio.pvzreborn.block;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.data.provider.PVZBlockStateProvider;
import com.xstudio.pvzreborn.data.provider.PVZBlockTagsProvider;
import net.minecraft.ChatFormatting;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @author superhelo
 */
public class PVZBlockBase extends Block {

	private final String name;

	public PVZBlockBase(Properties pProperties, String name) {
		super(pProperties);
		this.name = name;
	}

	@Override
	public void appendHoverText(@NotNull ItemStack pStack, @Nullable BlockGetter pLevel, @NotNull List<Component> pTooltip, @NotNull TooltipFlag pFlag) {
		String pKey = "block." + PVZReborn.MOD_ID + "." + name + ".tooltip.desc";
		if (I18n.exists(pKey)) {
			TranslatableComponent trans = new TranslatableComponent(pKey);
			pTooltip.add(trans.withStyle(ChatFormatting.GOLD));
		}
	}

	/**
	 * @author Bread_NiceCat
	 */
	protected void mineableByPickaxe() {
		PVZBlockTagsProvider.mineableByPickaxe.add(this);
	}

	/**
	 * @author Bread_NiceCat
	 */
	protected void directional() {
		PVZBlockStateProvider.horizontal.add(this);
	}

}
