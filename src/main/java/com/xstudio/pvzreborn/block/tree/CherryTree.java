package com.xstudio.pvzreborn.block.tree;

import com.xstudio.pvzreborn.block.tree.grower.CherryTreeGrower;
import net.minecraft.world.level.block.SaplingBlock;

public class CherryTree extends SaplingBlock {
    public CherryTree(Properties properties) {
        super(new CherryTreeGrower(), properties);
    }

}
