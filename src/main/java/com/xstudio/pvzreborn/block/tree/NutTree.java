package com.xstudio.pvzreborn.block.tree;

import com.xstudio.pvzreborn.block.tree.grower.NutTreeGrower;
import net.minecraft.world.level.block.SaplingBlock;

public class NutTree extends SaplingBlock {

    public NutTree(Properties pProperties) {
        super(new NutTreeGrower(), pProperties);
    }

}
