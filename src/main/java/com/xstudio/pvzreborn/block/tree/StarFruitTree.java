package com.xstudio.pvzreborn.block.tree;

import com.xstudio.pvzreborn.block.tree.grower.StarFruitTreeGrower;
import net.minecraft.world.level.block.SaplingBlock;

public class StarFruitTree extends SaplingBlock {

    public StarFruitTree(Properties pProperties) {
        super(new StarFruitTreeGrower(), pProperties);
    }

}
