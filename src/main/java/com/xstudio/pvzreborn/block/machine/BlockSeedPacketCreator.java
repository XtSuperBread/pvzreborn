package com.xstudio.pvzreborn.block.machine;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.block.entity.BlockEntitySeedPacketCreator;
import com.xstudio.pvzreborn.gui.container.ContainerSeedPacketCreator;
import com.xstudio.pvzreborn.gui.container.PVZMenuProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;
import org.jetbrains.annotations.Nullable;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Consumer;

/**
 * @author superhelo
 */
@ParametersAreNonnullByDefault
public class BlockSeedPacketCreator extends PVZBlockMachine {

	public BlockSeedPacketCreator() {
        super(Properties.of(Material.STONE).noOcclusion().strength(2.0f, 4.0f).requiresCorrectToolForDrops(), BlockNames.SEED_PACKET_CREATOR);
        this.registerDefaultState(this.stateDefinition.any().setValue(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH));
		mineableByPickaxe();
		directional();
	}

	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
		if (pLevel.isClientSide) {
			return getTicker((creator) -> creator.checkWhetherWork(creator.getInputHandler().getAllStacks()));
		}
		return getTicker(BlockEntitySeedPacketCreator::tick);
	}

	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
		return new BlockEntitySeedPacketCreator(pos, state);
	}

	@Override
	public MenuProvider createMenuProvider(Level pLevel, BlockPos pPos, Player pPlayer) {
		return new PVZMenuProvider<>(guiTransKey, (containerId, inventory) -> new ContainerSeedPacketCreator(containerId, inventory, pPos));
	}

	private <T extends BlockEntity> BlockEntityTicker<T> getTicker(Consumer<BlockEntitySeedPacketCreator> consumer) {
		return (level, pos, state, blockEntity) -> {
			if (level.getBlockEntity(pos) instanceof BlockEntitySeedPacketCreator creator) {
				consumer.accept(creator);
			}
		};
	}

}
