package com.xstudio.pvzreborn.block;

public class BlockNames {

    // machine
    public static final String SEED_PACKET_CREATOR = "seed_packet_creator";
    public static final String POLAR_CRAFTING_TABLE = "polar_crafting_table";

    // crop
    public static final String CLOVER = "clover";
    public static final String CABBAGE = "cabbage";
    public static final String GARLIC = "garlic";
    public static final String WHITE_TURNIP = "white_turnip";
    public static final String CATTAIL = "cattail";
    public static final String PEA_SEEDLINGS = "pea_seedlings";

    // crop block
    public static final String SQUASH_BLOCK = "squash_block";

    // tree
    public static final String CHERRY_SAPLING = "cherry_sapling";
    public static final String NUT_SAPLING = "nut_sapling";
    public static final String STAR_FRUIT_SAPLING = "star_fruit_sapling";

}
