package com.xstudio.pvzreborn.block.crop;

import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import org.jetbrains.annotations.NotNull;

public class AgeThreeCropBlock extends CropBlock {

    public static final int MAX_AGE = 3;

    public static final IntegerProperty AGE_4 = IntegerProperty.create("age", 0, 4);

    public AgeThreeCropBlock(Properties pProperties) {
        super(pProperties);
    }

    @NotNull
    @Override
    public IntegerProperty getAgeProperty() {
        return BlockStateProperties.AGE_3;
    }

    @Override
    protected void createBlockStateDefinition(Builder<Block, BlockState> pBuilder) {
        pBuilder.add(BlockStateProperties.AGE_3);
    }

    @Override
    public int getMaxAge() {
        return MAX_AGE;
    }

    protected int getBonemealAgeIncrease(@NotNull Level pLevel) {
        return super.getBonemealAgeIncrease(pLevel) / 3;
    }

}
