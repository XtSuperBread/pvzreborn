package com.xstudio.pvzreborn;

import com.xstudio.pvzreborn.block.BlockManager;
import com.xstudio.pvzreborn.block.entity.BlockEntityManager;
import com.xstudio.pvzreborn.capability.CapManager;
import com.xstudio.pvzreborn.entity.EntityManager;
import com.xstudio.pvzreborn.gui.container.ContainerManager;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.loot.LootModifierManager;
import com.xstudio.pvzreborn.network.ManagerNetwork;
import com.xstudio.pvzreborn.recipe.RecipeManager;
import com.xstudio.pvzreborn.sound.SoundManager;
import com.xstudio.pvzreborn.world.WorldGenManager;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

@Mod(PVZReborn.MOD_ID)
public class PVZReborn {
	public static final String MOD_ID = "pvzreborn";
	public static final String MOD_ID_WITH_COLON = MOD_ID + ":";
	public static final Logger LOGGER = LogManager.getLogger(MOD_ID);

	public PVZReborn() {
		LOGGER.info("PVZ:Reborn Begins to Construct...");
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        CapManager.init(bus);
		ItemManager.init(bus);
        BlockManager.init(bus);
        SoundManager.init(bus);
		EntityManager.init(bus);
		RecipeManager.init(bus);
		ManagerNetwork.init();
		WorldGenManager.init(bus);
		ContainerManager.init(bus);
		BlockEntityManager.init(bus);
		LootModifierManager.init(bus);
		LOGGER.info("PVZ:Reborn Construct Finished.");
	}

    /**
     * @author Bread_NiceCat
     */
    @NotNull
    public static ResourceLocation prefix(String id) {
        return new ResourceLocation(MOD_ID, id);
    }

    /**
     * @return modid_id
     */
    @NotNull
    public static String prefixWithUnderline(String id) {
        return MOD_ID + "_" + id;
    }

}
