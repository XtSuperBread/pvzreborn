package com.xstudio.pvzreborn.utils;

import com.google.gson.JsonObject;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;
import com.xstudio.pvzreborn.tab.PVZRebornTab;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

public class Utils {

	private Utils() {
	}

	public static JsonObject serializeStack(@NotNull ItemStack stack) {
		CompoundTag nbt = stack.serializeNBT();
		renameTag(nbt, "Count", "count");
        renameTag(nbt, "id", "item");
		renameTag(nbt, "tag", "nbt");
		Dynamic<Tag> dyn = new Dynamic<>(NbtOps.INSTANCE, nbt);
		return dyn.convert(JsonOps.INSTANCE).getValue().getAsJsonObject();
	}

	public static void renameTag(@NotNull CompoundTag nbt, String oldName, String newName) {
		Tag tag = nbt.get(oldName);
		if (tag != null) {
			nbt.remove(oldName);
			nbt.put(newName, tag);
		}
	}

	public static boolean allTrue(boolean @NotNull [] array) {
		for (boolean b : array) {
			if (!b) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static Item.Properties defaultItemProperties() {
		return new Item.Properties().tab(PVZRebornTab.getInstance());
	}

}
