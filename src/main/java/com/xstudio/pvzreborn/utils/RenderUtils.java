package com.xstudio.pvzreborn.utils;

import com.xstudio.pvzreborn.PVZReborn;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/17 18:12
 */
@OnlyIn(Dist.CLIENT)
public class RenderUtils {
	/**
	 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
	 */
	public static ResourceLocation getTextureByModelLayerLocation(ModelLayerLocation modelLayer) {
		return entityTexLoc(modelLayer.getModel().getPath());
	}

	/**
	 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
	 */
	public static ResourceLocation entityTexLoc(String name) {
		return PVZReborn.prefix("textures/entity/" + name + ".png");
	}

}
