package com.xstudio.pvzreborn.utils.registry;

import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * 第一个参数为名称,
 * 第二个参数为属性
 * 第三个为注册行为
 */
public interface RegistrationAction<P, T> extends BiFunction<String, P, Supplier<? extends T>> {
}
