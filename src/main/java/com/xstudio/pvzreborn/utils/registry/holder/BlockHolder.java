package com.xstudio.pvzreborn.utils.registry.holder;

import com.xstudio.pvzreborn.utils.registry.RegistrationAction;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.material.Material;

public class BlockHolder extends Holder<Properties, Block> {

    private boolean registerBlockItemNow = true;

    public BlockHolder(String name, RegistrationAction<Properties, ? extends Block> function) {
        super(name, function);
        this.properties = Properties.of(Material.STONE);
    }

    public boolean isRegisteringBlockItemNow() {
        return registerBlockItemNow;
    }

    public BlockHolder setDoNotRegisterBlockItemNow() {
        this.registerBlockItemNow = false;
        return this;
    }

}
