package com.xstudio.pvzreborn.utils.registry.holder;

import com.xstudio.pvzreborn.utils.registry.RegistrationAction;

import java.util.function.Supplier;

public class Holder<P, T> {

    /**
     * 元素名称
     */
    protected final String name;
    /**
     * 注册行为
     */
    protected final RegistrationAction<P, ? extends T> function;
    /**
     * 属性
     */
    protected P properties;

    public Holder(String name, RegistrationAction<P, ? extends T> function) {
        this.name = name;
        this.function = function;
    }

    public Supplier<? extends T> getSupplier() {
        return function.apply(name, properties);
    }

    public P getProperties() {
        return properties;
    }

    public void setProperties(P properties) {
        this.properties = properties;
    }

}
