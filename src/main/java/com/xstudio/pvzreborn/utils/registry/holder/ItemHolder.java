package com.xstudio.pvzreborn.utils.registry.holder;

import com.xstudio.pvzreborn.utils.Utils;
import com.xstudio.pvzreborn.utils.registry.RegistrationAction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Item.Properties;

public class ItemHolder extends Holder<Item.Properties, Item> {

    private boolean isHoldingBlockItem;

    public ItemHolder(String name, RegistrationAction<Properties, ? extends Item> function) {
        super(name, function);
        this.properties = Utils.defaultItemProperties();
    }

    public boolean isHoldingBlockItem() {
        return this.isHoldingBlockItem;
    }

    public ItemHolder setHoldBlockItem() {
        this.isHoldingBlockItem = true;
        return this;
    }

}
