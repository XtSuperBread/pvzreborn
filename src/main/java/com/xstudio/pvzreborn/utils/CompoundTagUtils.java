package com.xstudio.pvzreborn.utils;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.NumericTag;
import net.minecraft.nbt.Tag;
import org.jetbrains.annotations.NotNull;

public class CompoundTagUtils {

	private CompoundTagUtils() {
	}

	public static Number getNumericTagValueOrDefault(@NotNull CompoundTag compoundTag, String name, Number def) {
		Tag tag = compoundTag.get(name);
		if (tag instanceof NumericTag numericTag) {
			return numericTag.getAsNumber();
		}
		return def;
	}

	public static int getIntValueOrDefault(@NotNull CompoundTag compoundTag, String name, int def) {
		Tag tag = compoundTag.get(name);
		if (tag instanceof IntTag intTag) {
			return intTag.getAsInt();
		}
		return def;
	}

	public static String getStringValueOrDefault(CompoundTag tag, String name, String defaultValue) {
		if (tag.contains(name)) {
			return tag.get(name).getAsString();
		}
		return defaultValue;
	}

}
