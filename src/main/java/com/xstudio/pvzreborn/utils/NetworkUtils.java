package com.xstudio.pvzreborn.utils;

import com.xstudio.pvzreborn.capability.DaveCoin;
import com.xstudio.pvzreborn.capability.PlantFood;
import com.xstudio.pvzreborn.capability.Sunshine;
import com.xstudio.pvzreborn.network.DaveCoinPacket;
import com.xstudio.pvzreborn.network.PVZPacketBase;
import com.xstudio.pvzreborn.network.PlantFoodPacket;
import com.xstudio.pvzreborn.network.SunshinePacket;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.util.LazyOptional;

public class NetworkUtils {

	public static PVZPacketBase createPacketByName(String name, LazyOptional<?> opt) {
		return switch (name) {
			case Sunshine.NAME -> new SunshinePacket(opt.cast());
			case DaveCoin.NAME -> new DaveCoinPacket(opt.cast());
			case PlantFood.NAME -> new PlantFoodPacket(opt.cast());
			default -> throw new RuntimeException("unknown packet:" + name);
		};
	}

	/**
	 * 只在客户端环境调用
	 *
	 * @param daveCoin 发送至客户端的戴夫币数量
	 */
	public static void syncDaveCoinData(int daveCoin) {
		CapabilityUtils.getDaveCoin(Minecraft.getInstance().player).ifPresent(daveCoin1 -> daveCoin1.setValue(daveCoin));
	}

	/**
	 * 只在客户端环境调用
	 *
	 * @param sunshine 发送至客户端的阳光数量
	 */
	public static void syncSunshineData(int sunshine) {
		CapabilityUtils.getSunshine(Minecraft.getInstance().player).ifPresent(sunshine1 -> sunshine1.setValue(sunshine));
	}

	/**
	 * 只在客户端环境调用
	 *
	 * @param plantFood 发送至客户端的能量豆数量
	 */
	public static void syncPlantFoodData(int plantFood) {
		CapabilityUtils.getPlantFood(Minecraft.getInstance().player).ifPresent(plantFood1 -> plantFood1.setValue(plantFood));
	}

}
