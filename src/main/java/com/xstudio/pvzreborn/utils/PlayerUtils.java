package com.xstudio.pvzreborn.utils;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import org.apache.logging.log4j.core.util.UuidUtil;

import javax.annotation.Nonnull;

/**
 * @author Bread_NiceCat
 */
public class PlayerUtils {

	private PlayerUtils() {
	}

	/**
	 * 匹配玩家装备是否是指定的装备,null则不匹配
	 *
	 * @return true if 需要匹配的槽位均匹配
	 * @author Bread_NiceCat
	 */
	public static boolean isWear(Player player, ItemStack head, ItemStack chest, ItemStack legs, ItemStack feet) {
		return (head != null && player.getItemBySlot(EquipmentSlot.HEAD).is(head.getItem()))
				&& (chest != null && player.getItemBySlot(EquipmentSlot.CHEST).is(chest.getItem()))
				&& (legs != null && player.getItemBySlot(EquipmentSlot.LEGS).is(legs.getItem()))
				&& (feet != null && player.getItemBySlot(EquipmentSlot.FEET).is(feet.getItem()));
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static void sendText(Player player, String text) {
		sendText(player, new TextComponent(text));
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static void sendText(@Nonnull Player player, Component text) {
		player.sendMessage(text, UuidUtil.getTimeBasedUuid());
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static void sendI18nText(Player player, String text, Object... pArgs) {
		sendText(player, new TranslatableComponent(text, pArgs));
	}

}
