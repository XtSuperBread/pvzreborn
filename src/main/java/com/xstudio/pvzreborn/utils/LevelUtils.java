package com.xstudio.pvzreborn.utils;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Position;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import org.jetbrains.annotations.NotNull;


public class LevelUtils {

	private LevelUtils() {
	}

	/**
	 * @author superhelo
	 */
	public static void spawnItemEntity(Level level, Position pos, ItemStack stack) {
		level.addFreshEntity(newItemEntity(level, pos, stack));
	}

	/**
	 * @author superhelo
	 */
	public static void spawnItemEntity(Level level, BlockPos pos, ItemStack stack) {
		level.addFreshEntity(newItemEntity(level, pos, stack));
	}

	/**
	 * @author superhelo
	 */
	public static ItemEntity newItemEntity(Level level, Position pos, ItemStack stack) {
		return new ItemEntity(level, pos.x(), pos.y(), pos.z(), stack);
	}

	/**
	 * @author superhelo
	 */
	public static ItemEntity newItemEntity(Level level, BlockPos pos, ItemStack stack) {
		return new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(), stack);
	}

	/**
	 * @author superhelo
	 */
	public static ItemEntity newItemEntity(Level level, BlockPos pos, Item item) {
		return new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(), item.getDefaultInstance());
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static boolean canBlockSeeSun(Level level, BlockPos pBlockPos) {
		return isDay(level) && level.getBrightness(LightLayer.SKY, pBlockPos.above()) >= level.getMaxLightLevel();
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static boolean isDay(Level level) {
		return level.getDayTime() % 24000 <= 12000;
	}

	/**
	 * @author Bread_NiceCat
	 */
	public static boolean delay(@NotNull Level level, float sec) {
		return level.getDayTime() % (sec * 20) == 0;
	}

}
