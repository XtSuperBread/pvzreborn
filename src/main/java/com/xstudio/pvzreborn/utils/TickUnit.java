package com.xstudio.pvzreborn.utils;

/**
 * @author Bread_NiceCat
 */
public class TickUnit {

	private TickUnit() {
	}

	public static float toSecond(int tick) {
		return MathUtils.exact(tick / 20f, 2);
	}

	public static int toTick(float sec) {
		return Math.round(sec * 20);
	}

}
