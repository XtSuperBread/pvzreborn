package com.xstudio.pvzreborn.utils;

public class MathUtils {

	/**
	 * @param precision 精确到小数点后n位
	 *                  精确小数点
	 * @author Bread_NiceCat
	 */
	public static float exact(float f, int precision) {
		String[] sp = String.valueOf(f).split("[.]");//以‘.’分开
		return Float.parseFloat(sp[0] + "." + sp[1].substring(0, Math.min(precision, sp[1].length())));
	}

}
