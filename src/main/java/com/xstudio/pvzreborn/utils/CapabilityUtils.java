package com.xstudio.pvzreborn.utils;

import com.xstudio.pvzreborn.capability.CapManager;
import com.xstudio.pvzreborn.capability.DaveCoin;
import com.xstudio.pvzreborn.capability.IEditable;
import com.xstudio.pvzreborn.capability.PlantFood;
import com.xstudio.pvzreborn.capability.Sunshine;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;

public class CapabilityUtils {

    private CapabilityUtils() {
    }

    @NotNull
    public static LazyOptional<Sunshine> getSunshine(@NotNull Player player) {
        return player.getCapability(CapManager.SUNSHINE);
    }

    @NotNull
    public static LazyOptional<DaveCoin> getDaveCoin(@NotNull Player player) {
        return player.getCapability(CapManager.DAVE_COIN);
    }

    @NotNull
    public static LazyOptional<PlantFood> getPlantFood(@NotNull Player player) {
        return player.getCapability(CapManager.PLANT_FOOD);
    }

    public static LazyOptional<? extends IEditable<?>> getEditorByName(String name, @NotNull Player player) {
        return switch (name) {
            case Sunshine.NAME -> getSunshine(player);
            case DaveCoin.NAME -> getDaveCoin(player);
            case PlantFood.NAME -> getPlantFood(player);
            default -> LazyOptional.empty();
        };
    }

}
