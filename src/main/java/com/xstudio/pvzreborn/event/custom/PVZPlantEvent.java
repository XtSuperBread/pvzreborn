package com.xstudio.pvzreborn.event.custom;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.eventbus.api.Cancelable;

import javax.annotation.Nonnull;

public abstract class PVZPlantEvent extends PVZEventBase {

	private final BlockPos pos;

	protected PVZPlantEvent(@Nonnull BlockPos pos) {
		this.pos = pos;
	}

	@Nonnull
	public BlockPos getPos() {
		return pos;
	}

	@Cancelable
	public static class PlantPlantedEvent extends PVZPlantEvent {

		private final Player planter;

		public PlantPlantedEvent(BlockPos pos, Player planter) {
			super(pos);
			this.planter = planter;
		}

		public Player getPlanter() {
			return planter;
		}
	}

	public static class PlantDeadEvent extends PVZPlantEvent {

		public PlantDeadEvent(BlockPos pos) {
			super(pos);
		}

	}

}
