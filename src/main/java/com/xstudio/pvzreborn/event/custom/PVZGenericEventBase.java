package com.xstudio.pvzreborn.event.custom;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.GenericEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class PVZGenericEventBase<T> extends GenericEvent<T> {

    public PVZGenericEventBase() {
    }

    protected PVZGenericEventBase(Class<T> type) {
        super(type);
    }

    public boolean post() {
        return MinecraftForge.EVENT_BUS.post(this);
    }

    public boolean postModBus() {
        return FMLJavaModLoadingContext.get().getModEventBus().post(this);
    }

}
