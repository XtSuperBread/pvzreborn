package com.xstudio.pvzreborn.event.custom;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.Event;

public class PVZEventBase extends Event {

	public boolean post() {
		return MinecraftForge.EVENT_BUS.post(this);
	}

}
