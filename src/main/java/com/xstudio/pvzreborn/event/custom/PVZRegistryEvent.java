package com.xstudio.pvzreborn.event.custom;

import net.minecraftforge.eventbus.api.Cancelable;
import net.minecraftforge.fml.event.IModBusEvent;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class PVZRegistryEvent<T> extends PVZGenericEventBase<T> implements IModBusEvent {

    private final Kind kind;
    private final String name;
    private final String textureGroup;

    public PVZRegistryEvent(Kind kind, Class<T> clazz, String name, String textureGroup) {
        super(clazz);
        this.kind = kind;
        this.name = name;
        this.textureGroup = textureGroup;
    }

    public String getName() {
        return name;
    }

    public String getTextureGroup() {
        return textureGroup;
    }

    public Kind getKind() {
        return kind;
    }

    public enum Kind {
        BlockItem(net.minecraft.world.item.BlockItem.class),
        Item(net.minecraft.world.item.Item.class),
        Block(net.minecraft.world.level.block.Block.class);

        private final Class<?> clazz;

        Kind(Class<?> clazz) {
            this.clazz = clazz;
        }

        public Class<?> getClazz() {
            return clazz;
        }

    }

    @Cancelable
    public static class Pre<T> extends PVZRegistryEvent<T> {
        private Supplier<? extends T> supplier;

        public Pre(Kind kind, Class<T> clazz, String name, String textureGroup, Supplier<? extends T> supplier) {
            super(kind, clazz, name, textureGroup);
            this.supplier = supplier;
        }

        public Supplier<? extends T> getSupplier() {
            return supplier;
        }

        public void setNewSupplier(Supplier<? extends T> newSupplier) {
            this.supplier = newSupplier;
        }

    }

    public static class Post<T> extends PVZRegistryEvent<T> {

        private final RegistryObject<? extends T> object;

        public Post(Kind kind, Class<T> clazz, String name, String textureGroup, RegistryObject<? extends T> object) {
            super(kind, clazz, name, textureGroup);
            this.object = object;
        }

        public RegistryObject<? extends T> getObject() {
            return object;
        }

    }

}
