package com.xstudio.pvzreborn.event;

import com.xstudio.pvzreborn.cmd.PVZCommand;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;

@EventBusSubscriber
public final class ServerEventListener {

	@SubscribeEvent
	public static void onRegisterCommand(RegisterCommandsEvent event) {
		PVZCommand.init(event.getDispatcher());
	}

}
