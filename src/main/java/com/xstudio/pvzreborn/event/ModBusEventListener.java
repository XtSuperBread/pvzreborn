package com.xstudio.pvzreborn.event;

import com.xstudio.pvzreborn.entity.EntityManager;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;

@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD)
public class ModBusEventListener {

	@SuppressWarnings("unchecked")
	@SubscribeEvent
	public static void onAttributeCreate(EntityAttributeCreationEvent event) {
		EntityManager.getAttribute().forEach((entitySupplier, attribute) -> event.put((EntityType<? extends LivingEntity>) entitySupplier.get(), attribute.get().build()));
	}

}
