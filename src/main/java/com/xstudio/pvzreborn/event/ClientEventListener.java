package com.xstudio.pvzreborn.event;

import com.xstudio.pvzreborn.block.BlockManager;
import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.entity.EntityManager;
import com.xstudio.pvzreborn.entity.entities.misc.EntitySunshine;
import com.xstudio.pvzreborn.entity.entities.plant.EntityPlantWallNut;
import com.xstudio.pvzreborn.entity.models.ModelPlantWallNut;
import com.xstudio.pvzreborn.entity.renderer.RendererPlantWallNut;
import com.xstudio.pvzreborn.entity.renderer.RendererSunshine;
import com.xstudio.pvzreborn.gui.container.ContainerManager;
import com.xstudio.pvzreborn.gui.hud.ManagerOverlay;
import com.xstudio.pvzreborn.gui.screen.ScreenPolarCraftingTable;
import com.xstudio.pvzreborn.gui.screen.ScreenSeedPacketCreator;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.client.gui.OverlayRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

@EventBusSubscriber(value = Dist.CLIENT, bus = Bus.MOD)
public final class ClientEventListener {

    @SubscribeEvent
    public static void init(FMLClientSetupEvent event) {
        event.enqueueWork(() -> {
            MenuScreens.register(ContainerManager.getMenuType(BlockNames.SEED_PACKET_CREATOR), ScreenSeedPacketCreator::new);
            MenuScreens.register(ContainerManager.getMenuType(BlockNames.POLAR_CRAFTING_TABLE), ScreenPolarCraftingTable::new);
            RenderType cutout = RenderType.cutout();
            RenderType translucent = RenderType.translucent();
            Consumer<String> consumer = blockName -> ItemBlockRenderTypes.setRenderLayer(BlockManager.getBlock(blockName), cutout);
            BlockManager.getBlockStateModel("crop").forEach(consumer);
            BlockManager.getBlockStateModel("tree").forEach(consumer);
            BlockManager.getBlockStateModel("machine").forEach(blockName -> ItemBlockRenderTypes.setRenderLayer(BlockManager.getBlock(blockName), translucent));
            OverlayRegistry.registerOverlayAbove(ForgeIngameGui.VIGNETTE_ELEMENT, "pvz_base_hud", ManagerOverlay.PVZ_BASE_HUD);
        });
	}

	/**
	 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
	 */
	@SubscribeEvent
	public static void onRegisterLayers(EntityRenderersEvent.@NotNull RegisterLayerDefinitions event) {
		event.registerLayerDefinition(ModelPlantWallNut.LAYER_LOCATION, ModelPlantWallNut::createBodyLayer);
	}

	/**
	 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
	 */
	@SubscribeEvent
	public static void onRegisterRenderer(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(EntityManager.getEntity(EntityPlantWallNut.NAME), RendererPlantWallNut::new);
		event.registerEntityRenderer(EntityManager.getEntity(EntitySunshine.NAME), RendererSunshine::new);
	}

}
