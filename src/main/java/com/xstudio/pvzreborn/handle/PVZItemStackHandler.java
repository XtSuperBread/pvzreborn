package com.xstudio.pvzreborn.handle;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * @author superhelo
 */
public class PVZItemStackHandler extends ItemStackHandler {

	protected final BiConsumer<PVZItemStackHandler, Integer> consumer;

	public PVZItemStackHandler(BiConsumer<PVZItemStackHandler, Integer> consumer) {
		super();
		this.consumer = consumer;
	}

	public PVZItemStackHandler(int size, BiConsumer<PVZItemStackHandler, Integer> consumer) {
		super(size);
		this.consumer = consumer;
	}

	@Override
	protected void onContentsChanged(int slot) {
		consumer.accept(this, slot);
	}

	public List<ItemStack> getAllStacks() {
		int slots = getSlots();
		List<ItemStack> list = new ArrayList<>(slots);
		for (int i = 0; i < slots; i++) {
			ItemStack stack = getStackInSlot(i);
			if (!stack.isEmpty()) {
				list.add(stack);
			}
		}
		return list;
	}

}
