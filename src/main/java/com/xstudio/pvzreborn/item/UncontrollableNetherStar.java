package com.xstudio.pvzreborn.item;

import com.xstudio.pvzreborn.utils.Utils;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import org.jetbrains.annotations.NotNull;

public class UncontrollableNetherStar extends PVZItemBase {

	public UncontrollableNetherStar() {
		super(ItemNames.UNCONTROLLABLE_NETHER_STAR, Utils.defaultItemProperties().rarity(Rarity.RARE));
	}

	@Override
	public boolean isFoil(@NotNull ItemStack pStack) {
		return true;
	}

}
