package com.xstudio.pvzreborn.item;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.utils.Utils;
import net.minecraft.ChatFormatting;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author Bread_NiceCat
 */
public class PVZItemBase extends Item {

	protected final String name;
    protected final String langKey;

	/**
	 * @author Bread_NiceCat
	 */
	public PVZItemBase(String name) {
		this(name, Utils.defaultItemProperties());
	}

	/**
	 * @author Bread_NiceCat
	 */
	public PVZItemBase(String name, @NotNull Properties properties) {
		super(properties);
        this.name = name;
        this.langKey = "item." + PVZReborn.MOD_ID + "." + name + ".tooltip.desc";
	}

	/**
	 * @author Bread_NiceCat
	 */
	@Override
	public void appendHoverText(@NotNull ItemStack pStack, @Nullable Level pLevel, @NotNull List<Component> pTooltipComponents, @NotNull TooltipFlag pIsAdvanced) {
        if (I18n.exists(langKey)) {
            pTooltipComponents.add(new TranslatableComponent(langKey).withStyle(ChatFormatting.GOLD));
        }
    }

}
