package com.xstudio.pvzreborn.item;

public class ItemNames {

    // Common Item
    public static final String DARK_KNIGHT_SWORD = "dark_knight_sword";
    public static final String SUPERCRITICAL_FIRE = "supercritical_fire";
    public static final String EIGHTEENTH_STATE_ICE = "eighteenth_state_ice";
    public static final String ANCIENT_EYE = "ancient_eye";
    public static final String OVERLOADED_REDSTONE = "overloaded_redstone";
    public static final String REDSTONE_ARM = "redstone_arm";
    public static final String HUGE_PEA = "huge_pea";
    public static final String BEDROCK_DUST = "bedrock_dust";
    public static final String CREEPER_GLAND = "creeper_gland";
    public static final String SHARP_ENDER_CRYSTAL_SHARD = "sharp_ender_crystal_shard";
    public static final String ENTITY_LIGHT = "entity_light";
    public static final String IRON_GOLEM_SCULPTURE_SHARD = "iron_golem_sculpture_shard";
    public static final String COMPACT_THALLIUM_SULFATE = "compact_thallium_sulfate";
    public static final String UNCONTROLLABLE_NETHER_STAR = "uncontrollable_nether_star";
    // Test Tube
    public static final String EMPTY_TEST_TUBE = "empty_test_tube";
    public static final String WARPED_WART_TEST_TUBE = "warped_wart_test_tube";
    // MISC
    public static final String WISDOM_TREE_ESSENCE = "wisdom_tree_essence";
    public static final String PHOTOVOLTAIC_ALLOY = "photovoltaic_alloy";
    public static final String NATURAL_DIAMOND = "natural_diamond";
    public static final String RUBY = "ruby";
    // Coin
    public static final String SLIVER_COIN = "silver_coin";
    public static final String GOLDEN_COIN = "golden_coin";
    public static final String SHINING_DIAMOND = "shining_diamond";
    // Armor
    public static final String NATURAL_DIAMOND_HELMET = NATURAL_DIAMOND + "_helmet";
    public static final String NATURAL_DIAMOND_CHESTPLATE = NATURAL_DIAMOND + "_chestplate";
    public static final String NATURAL_DIAMOND_LEGGINGS = NATURAL_DIAMOND + "_leggings";
    public static final String NATURAL_DIAMOND_BOOTS = NATURAL_DIAMOND + "_boots";
    public static final String PHOTOVOLTAIC_ALLOY_HELMET = PHOTOVOLTAIC_ALLOY + "_helmet";
    public static final String PHOTOVOLTAIC_ALLOY_CHESTPLATE = PHOTOVOLTAIC_ALLOY + "_chestplate";
    public static final String PHOTOVOLTAIC_ALLOY_LEGGINGS = PHOTOVOLTAIC_ALLOY + "_leggings";
    public static final String PHOTOVOLTAIC_ALLOY_BOOTS = PHOTOVOLTAIC_ALLOY + "_boots";
    // Fruit
    public static final String CHERRY = "cherry";
    public static final String MARIGOLD = "marigold";
    public static final String TURNIP = "turnip";
    public static final String SQUASH = "squash";
    public static final String CORN = "corn";
    public static final String PEAPOD = "peapod";
    public static final String STARFRUIT = "starfruit";
    public static final String KASUALIX_POTATO = "kasualix_potato";
    public static final String NUT = "nut";
    public static final String PEPPER = "pepper";
    public static final String PEA = "pea";
    public static final String TOXICSHROOM = "toxicshroom";
    public static final String CHOMPER = "chomper";
    // suffix
    private static final String TEST_TUBE_SUFFIX = "_seed_liquid_extract";
    public static final String AID_TEST_TUBE = "aid" + TEST_TUBE_SUFFIX;
    public static final String CONTROL_TEST_TUBE = "control" + TEST_TUBE_SUFFIX;
    public static final String EXPLOSION_TEST_TUBE = "explosion" + TEST_TUBE_SUFFIX;
    public static final String FIRE_TEST_TUBE = "fire" + TEST_TUBE_SUFFIX;
    public static final String ICE_TEST_TUBE = "ice" + TEST_TUBE_SUFFIX;
    public static final String MAGIC_TEST_TUBE = "magic" + TEST_TUBE_SUFFIX;
    public static final String MECHANICAL_TEST_TUBE = "mechanical" + TEST_TUBE_SUFFIX;
    public static final String POISON_TEST_TUBE = "poison" + TEST_TUBE_SUFFIX;
    public static final String PROPELLANT_TEST_TUBE = "propellant" + TEST_TUBE_SUFFIX;
    public static final String SHARPNESS_TEST_TUBE = "sharpness" + TEST_TUBE_SUFFIX;
    public static final String SUNSHINE_TEST_TUBE = "sunshine" + TEST_TUBE_SUFFIX;
    public static final String THUNDER_TEST_TUBE = "thunder" + TEST_TUBE_SUFFIX;
    public static final String WARRIOR_TEST_TUBE = "warrior" + TEST_TUBE_SUFFIX;
    public static final String DARKNESS_TEST_TUBE = "darkness" + TEST_TUBE_SUFFIX;
    private static final String SEED_PACKET_SUFFIX = "_seed_packet";
    // Seed Packet
    public static final String WHITE_SEED_PACKET = "white" + SEED_PACKET_SUFFIX;
    public static final String GREEN_SEED_PACKET = "green" + SEED_PACKET_SUFFIX;
    public static final String BLUE_SEED_PACKET = "blue" + SEED_PACKET_SUFFIX;
    public static final String PURPLE_SEED_PACKET = "purple" + SEED_PACKET_SUFFIX;
    public static final String GOLDEN_SEED_PACKET = "golden" + SEED_PACKET_SUFFIX;
    public static final String RED_SEED_PACKET = "red" + SEED_PACKET_SUFFIX;
    public static final String WALLNUT_SEED_PACKET = "wallnut" + SEED_PACKET_SUFFIX;
    private static final String POLAR_DYE_SUFFIX = "_polar_dye";
    // Polar Dye
    public static final String WHITE_POLAR_DYE = "white" + POLAR_DYE_SUFFIX;
    public static final String GREEN_POLAR_DYE = "green" + POLAR_DYE_SUFFIX;
    public static final String BLUE_POLAR_DYE = "blue" + POLAR_DYE_SUFFIX;
    public static final String PURPLE_POLAR_DYE = "purple" + POLAR_DYE_SUFFIX;
    public static final String GOLDEN_POLAR_DYE = "golden" + POLAR_DYE_SUFFIX;
    public static final String RED_POLAR_DYE = "red" + POLAR_DYE_SUFFIX;

}
