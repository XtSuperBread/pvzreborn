package com.xstudio.pvzreborn.item.armor;

import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import com.xstudio.pvzreborn.utils.CapabilityUtils;
import com.xstudio.pvzreborn.utils.LevelUtils;
import com.xstudio.pvzreborn.utils.PlayerUtils;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class NaturalDiamondArmor extends PVZArmorItemBase {

	public NaturalDiamondArmor(ArmorMaterial pMaterial, EquipmentSlot pSlot) {
		super(pMaterial, pSlot);
	}

	private static boolean isCompletelyWear(Player player) {
		return PlayerUtils.isWear(player, ItemManager.getDefaultItemStack(ItemNames.NATURAL_DIAMOND_HELMET), ItemManager.getDefaultItemStack(ItemNames.NATURAL_DIAMOND_CHESTPLATE), ItemManager.getDefaultItemStack(ItemNames.NATURAL_DIAMOND_LEGGINGS), ItemManager.getDefaultItemStack(ItemNames.NATURAL_DIAMOND_BOOTS));
	}

	@Override
	public void onArmorTick(ItemStack stack, Level level, Player player) {
		if (LevelUtils.delay(level, 1f) && LevelUtils.canBlockSeeSun(level, player.getOnPos()) && isCompletelyWear(player)) {
			CapabilityUtils.getSunshine(player).ifPresent(sunshine -> sunshine.add(0.25f));
			player.addEffect(new MobEffectInstance(MobEffects.JUMP, 200));
			player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 200));
			player.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 200));
		}
	}

}
