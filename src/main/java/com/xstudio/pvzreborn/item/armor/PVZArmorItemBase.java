package com.xstudio.pvzreborn.item.armor;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.utils.Utils;
import net.minecraft.ChatFormatting;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class PVZArmorItemBase extends ArmorItem {

	public PVZArmorItemBase(ArmorMaterial pMaterial, EquipmentSlot pSlot, Properties pProperties) {
		super(pMaterial, pSlot, pProperties);
	}

	public PVZArmorItemBase(ArmorMaterial pMaterial, EquipmentSlot pSlot) {
		this(pMaterial, pSlot, Utils.defaultItemProperties());
	}


	/**
	 * @author Bread_NiceCat
	 */
	@Override
	public void appendHoverText(@NotNull ItemStack pStack, @Nullable Level pLevel, @NotNull List<Component> pTooltipComponents, @NotNull TooltipFlag pIsAdvanced) {
		super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
		String suit = "item.armor_suit." + PVZReborn.MOD_ID + "." + material.getName().split(":", 2)[1] + ".tooltip.desc";
		if (I18n.exists(suit)) {
			pTooltipComponents.add(new TranslatableComponent("item.armor_suit." + PVZReborn.MOD_ID + ".tooltip.desc").withStyle(ChatFormatting.GREEN));
			pTooltipComponents.add(new TextComponent("   ").append(new TranslatableComponent(suit)).withStyle(ChatFormatting.AQUA));
		}
	}

}
