package com.xstudio.pvzreborn.item;

import com.xstudio.pvzreborn.capability.Sunshine;
import com.xstudio.pvzreborn.entity.EntityManager;
import com.xstudio.pvzreborn.event.custom.PVZPlantEvent;
import com.xstudio.pvzreborn.tab.PVZRebornSeedTab;
import com.xstudio.pvzreborn.utils.CapabilityUtils;
import com.xstudio.pvzreborn.utils.CompoundTagUtils;
import com.xstudio.pvzreborn.utils.TickUnit;
import com.xstudio.pvzreborn.utils.Triple;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author Bread_NiceCat
 */
public class PVZPlantSeedPacketItem extends PVZItemBase {

	private final int coolDown;
	private final int reqSunshine;
	private final String entityName;

	public PVZPlantSeedPacketItem(String name, String entityName, int reqSunshine, int coolDownSec) {
		this(name, entityName, new Properties(), reqSunshine, coolDownSec);
	}

	public PVZPlantSeedPacketItem(String name, String entityName, Properties prop, int reqSunshine, int coolDownSec) {
		super(name, prop.tab(PVZRebornSeedTab.getInstance()));
		this.entityName = entityName;
		this.reqSunshine = reqSunshine;
		this.coolDown = TickUnit.toTick(coolDownSec);
	}

	@NotNull
	@Override
	public InteractionResult useOn(@NotNull UseOnContext pContext) {
		Level level = pContext.getLevel();
		Player player = pContext.getPlayer();
		BlockPos blockPos = pContext.getClickedPos();
		ItemStack itemInHand = pContext.getItemInHand();
		Triple<String, Integer, Integer> tags = getSeedPacketTag(itemInHand);

		if (!level.isClientSide && player != null && pContext.getClickedFace().equals(Direction.UP)) {
			int requiredSunshine = tags.second();
			Sunshine sunshine = CapabilityUtils.getSunshine(player).resolve().get();
			if (sunshine.getValue() >= requiredSunshine && level.getBlockState(blockPos).is(Blocks.GRASS_BLOCK) && level.getBlockState(blockPos.above()).is(Blocks.AIR) && !new PVZPlantEvent.PlantPlantedEvent(blockPos, player).post()) {
				itemInHand.shrink(1);
				sunshine.remove(requiredSunshine, (ServerPlayer) player);
				player.getCooldowns().addCooldown(this, tags.third());
				Entity pEntity = EntityManager.getEntity(tags.first()).create(level);
				pEntity.setPos(Vec3.atCenterOf(blockPos).add(0, 0.5, 0));
				level.addFreshEntity(pEntity);
				return InteractionResult.SUCCESS;
			}
			return InteractionResult.FAIL;
		}
		return InteractionResult.PASS;
	}

	@NotNull
	@Override
	public Component getName(@NotNull ItemStack pStack) {
		return new TranslatableComponent("item.pvzreborn.plant_seed_packet");
	}

	@Override
	public void appendHoverText(@NotNull ItemStack pStack, @Nullable Level pLevel, @NotNull List<Component> pTooltipComponents, @NotNull TooltipFlag pIsAdvanced) {
		Triple<String, Integer, Integer> tags = getSeedPacketTag(pStack);
		pTooltipComponents.add(new TranslatableComponent("item.pvzreborn.seed_packet.liveOn.tooltip.desc").withStyle(ChatFormatting.YELLOW, ChatFormatting.BOLD));
		pTooltipComponents.add(new TranslatableComponent("item.pvzreborn.seed_packet.summon.tooltip.desc", new TranslatableComponent("entity.pvzreborn." + tags.first()).withStyle(ChatFormatting.BOLD).getString()).withStyle(ChatFormatting.GREEN));
		pTooltipComponents.add(new TranslatableComponent("item.pvzreborn.seed_packet.reqSunshine.tooltip.desc", tags.second()).withStyle(ChatFormatting.GREEN));
		pTooltipComponents.add(new TranslatableComponent("item.pvzreborn.seed_packet.cds.tooltip.desc", TickUnit.toSecond(tags.third())).withStyle(ChatFormatting.GREEN));
	}

	private Triple<String, Integer, Integer> getSeedPacketTag(ItemStack stack) {
		CompoundTag tag = stack.getOrCreateTag();
		int reqSunshine = CompoundTagUtils.getIntValueOrDefault(tag, "reqSunshine", this.reqSunshine);
		int coolDown = CompoundTagUtils.getIntValueOrDefault(tag, "cdt", this.coolDown);
		String entityName = CompoundTagUtils.getStringValueOrDefault(tag, "plant", this.entityName);
		return Triple.of(entityName, reqSunshine, coolDown);
	}

}
