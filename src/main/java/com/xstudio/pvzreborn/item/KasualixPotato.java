package com.xstudio.pvzreborn.item;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

public class KasualixPotato extends PVZItemBase {

	public KasualixPotato() {
		super(ItemNames.KASUALIX_POTATO, new Properties().rarity(Rarity.UNCOMMON).food(new FoodProperties.Builder().nutrition(20).saturationMod(20).fast().alwaysEat().build()));
	}

	@NotNull
	@Override
	public ItemStack finishUsingItem(@NotNull ItemStack pStack, @NotNull Level pLevel, @NotNull LivingEntity pLivingEntity) {
        if (!pLevel.isClientSide) {
            pLevel.explode(pLivingEntity, pLivingEntity.getX(), pLivingEntity.getY(), pLivingEntity.getZ(), 4f, false, Explosion.BlockInteraction.NONE);
        }
        return super.finishUsingItem(pStack, pLevel, pLivingEntity);
    }

}
