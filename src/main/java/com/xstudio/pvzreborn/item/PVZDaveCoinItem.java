package com.xstudio.pvzreborn.item;

import com.xstudio.pvzreborn.utils.CapabilityUtils;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class PVZDaveCoinItem extends PVZItemBase {

	private final int add;

	public PVZDaveCoinItem(String name, int add) {
		super(name);
		this.add = add;
	}

	@NotNull
	@Override
	public InteractionResultHolder<ItemStack> use(@NotNull Level pLevel, @NotNull Player pPlayer, InteractionHand pUsedHand) {
		ItemStack itemInHand = pPlayer.getItemInHand(pUsedHand);
		if (pPlayer instanceof ServerPlayer sPlayer) {
			itemInHand.shrink(1);
			CapabilityUtils.getDaveCoin(pPlayer).ifPresent(daveCoin -> daveCoin.add(this.add, sPlayer));
		}
		return InteractionResultHolder.success(itemInHand);
	}

	@Override
	public void appendHoverText(@NotNull ItemStack pStack, @Nullable Level pLevel, @NotNull List<Component> pTooltipComponents, @NotNull TooltipFlag pIsAdvanced) {
		pTooltipComponents.add(new TranslatableComponent("item.pvzreborn.coin.tooltip.desc", add).withStyle(ChatFormatting.GREEN));
		super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
	}

}
