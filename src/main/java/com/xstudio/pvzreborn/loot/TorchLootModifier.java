package com.xstudio.pvzreborn.loot;

import com.google.gson.JsonObject;
import com.xstudio.pvzreborn.item.ItemManager;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds.Doubles;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LocationCheck;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.common.loot.LootTableIdCondition;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TorchLootModifier extends LootModifier {

    public static final String NAME = "torch";

    public TorchLootModifier() {
        super(new LootItemCondition[]{
                LootTableIdCondition.builder(new ResourceLocation("blocks/torch")).build(),
                LocationCheck.checkLocation(LocationPredicate.Builder.location().setY(Doubles.between(319, 325))).build()
        });
    }

    public TorchLootModifier(LootItemCondition[] condition) {
        super(condition);
    }

    @NotNull
    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
	    generatedLoot.add(ItemManager.getDefaultItemStack("entity_light"));
        return generatedLoot;
    }

    public static class Serializer extends GlobalLootModifierSerializer<TorchLootModifier> {

        @Override
        public TorchLootModifier read(ResourceLocation location, JsonObject object, LootItemCondition[] condition) {
            return new TorchLootModifier(condition);
        }

        @Override
        public JsonObject write(TorchLootModifier instance) {
            return makeConditions(instance.conditions);
        }

    }

}
