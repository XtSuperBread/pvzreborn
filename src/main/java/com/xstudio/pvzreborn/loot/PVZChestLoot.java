package com.xstudio.pvzreborn.loot;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import net.minecraft.data.loot.ChestLoot;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import static net.minecraft.world.item.Items.*;

/**
 * @author Bread_NiceCat
 */
public class PVZChestLoot extends ChestLoot {
	private static final Map<ResourceLocation, LootTable.Builder> LOOTS = new HashMap<>();

	public static void registerLoot(String chestLootName, int poolmin, int poolmax, SimpleItemGenProp... prop) {
		LootPool.Builder lootPool = LootPool.lootPool().setRolls(UniformGenerator.between(poolmin, poolmax));
		for (SimpleItemGenProp genProp : prop) {
			genProp.addMe(lootPool);
		}
		LOOTS.put(prefix(chestLootName), LootTable.lootTable().withPool(lootPool));
	}

	/**
	 * @author Bread_NiceCat
	 */
	// 在此注册战利品表
	private static void preinit() {
		registerLoot("master_statues", 3, 5, of(COPPER_INGOT, 5, 2, 5), of(REDSTONE, 5, 3, 5), of(STONE, 5, 1, 10), of(QUARTZ, 5, 2), of(REDSTONE_TORCH, 5, 3), of(ItemManager.getItem(ItemNames.OVERLOADED_REDSTONE), 10, 16), of(REPEATER, 3, 1, 2), of(COMPARATOR, 3, 2));
		registerLoot("nether_tower", 2, 4, of(BLACKSTONE, 6, 12), of(FLINT_AND_STEEL, 6, 2), of(ItemManager.getItem(ItemNames.SUPERCRITICAL_FIRE), 5, 6, 12), of(AMETHYST_SHARD, 5, 1, 12));
		registerLoot("ice_tower", 2, 4, of(BLUE_ICE, 6, 8, 16), of(SNOWBALL, 6, 1, 12), of(ItemManager.getItem(ItemNames.EIGHTEENTH_STATE_ICE), 3, 8, 16), of(GOLD_BLOCK, 2, 1, 2), of(DIAMOND_BLOCK, 1, 1));
		registerLoot("iron_golem_statues", 2, 4, of(MOSS_BLOCK, 6, 1, 5), of(POPPY, 5, 2, 6), of(ItemManager.getItem(ItemNames.IRON_GOLEM_SCULPTURE_SHARD), 8, 2, 3), of(IRON_BLOCK, 3, 1, 2), of(TOTEM_OF_UNDYING, 1, 1));
		registerLoot("knight_tomb", 5, 8, of(STRING, 10, 2, 3), of(GLASS_BOTTLE, 10, 1, 2), of(GOLD_NUGGET, 8, 1, 3), of(OBSIDIAN, 8, 1, 2), of(WRITABLE_BOOK, 8, 1), of(CHAIN, 8, 2, 3), of(IRON_SWORD, 6, 1, 2), of(ItemManager.getItem(ItemNames.DARK_KNIGHT_SWORD), 6, 1, 3), of(ItemManager.getItem(ItemNames.COMPACT_THALLIUM_SULFATE), 8, 8, 10), of(BLAZE_POWDER, 5, 1, 2), of(ENDER_EYE, 4, 1, 2), of(DIAMOND, 4, 1, 2), of(NETHERITE_SCRAP, 4, 1));
	}

	private static ResourceLocation prefix(String chestLootName) {
		return PVZReborn.prefix("chests/" + chestLootName);
	}

	private static SimpleItemGenProp of(ItemLike item, int weight, int count) {
		return new SimpleItemGenProp(item, weight, count);
	}

	private static SimpleItemGenProp of(ItemLike item, int weight, int min, int max) {
		return new SimpleItemGenProp(item, weight, min, max);
	}

	@Override
	public void accept(@NotNull BiConsumer<ResourceLocation, LootTable.Builder> emptyLootMap) {
		preinit();
		for (Map.Entry<ResourceLocation, LootTable.Builder> builderEntry : LOOTS.entrySet()) {
			emptyLootMap.accept(builderEntry.getKey(), builderEntry.getValue());
		}
	}

	/**
	 * @author Bread_NiceCat
	 */
	private static class SimpleItemGenProp {
		private final ItemLike item;
		private final int weight;
		private final float[] range = new float[2];

		SimpleItemGenProp(ItemLike item, int weight, int count) {
			this(item, weight, count, count);
		}

		SimpleItemGenProp(ItemLike item, int weight, int min, int max) {
			this.item = item;
			this.weight = weight;
			this.range[0] = min;
			this.range[1] = max;
		}

		public LootPool.Builder addMe(LootPool.Builder builder) {
			return builder.add(LootItem.lootTableItem(item).setWeight(weight).apply(SetItemCountFunction.setCount(UniformGenerator.between(range[0], range[1]))));
		}

	}

}
