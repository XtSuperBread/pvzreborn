package com.xstudio.pvzreborn.loot;

import com.xstudio.pvzreborn.block.BlockManager;
import net.minecraft.data.loot.BlockLoot;
import net.minecraft.world.level.block.Block;

import javax.annotation.Nonnull;
import java.util.List;

public class PVZBlockLoot extends BlockLoot {

	private static final List<Block> allBlocks = BlockManager.getAllBlocks();

    @Override
    protected void addTables() {
        allBlocks.forEach(this::dropSelf);
    }

    @Nonnull
    @Override
    protected Iterable<Block> getKnownBlocks() {
        return allBlocks;
    }

}
