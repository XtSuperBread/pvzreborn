package com.xstudio.pvzreborn.tab;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.item.ItemManager;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

public class PVZRebornSeedTab extends CreativeModeTab {
	private static final PVZRebornSeedTab INSTANCE = new PVZRebornSeedTab();

	private PVZRebornSeedTab() {
		super(PVZReborn.MOD_ID + ".seed_packet");
	}

	public static PVZRebornSeedTab getInstance() {
		return INSTANCE;
	}

	@Override
	public @NotNull ItemStack makeIcon() {
		return ItemManager.getDefaultItemStack("red_seed_packet");
	}
}
