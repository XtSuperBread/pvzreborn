package com.xstudio.pvzreborn.tab;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.item.ItemManager;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

public class PVZRebornTab extends CreativeModeTab {
    private static final PVZRebornTab INSTANCE = new PVZRebornTab();

    private PVZRebornTab() {
        super(PVZReborn.MOD_ID);
    }

    public static PVZRebornTab getInstance() {
        return INSTANCE;
    }

    @NotNull
    @Override
    public ItemStack makeIcon() {
        return ItemManager.getDefaultItemStack("huge_pea");
    }

}
