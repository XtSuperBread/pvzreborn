package com.xstudio.pvzreborn.data.custom;

import com.google.gson.JsonObject;
import com.xstudio.pvzreborn.recipe.RecipeBase;
import com.xstudio.pvzreborn.recipe.RecipeManager;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeSerializer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

public abstract class BaseFinishedRecipe<T extends RecipeBase> implements FinishedRecipe, ISaveable {

    protected final T recipe;

    public BaseFinishedRecipe(T recipe) {
        this.recipe = recipe;
    }

    public abstract String getTypeId();

    @NotNull
    @Override
    public ResourceLocation getId() {
        return recipe.getId();
    }

    @NotNull
    @Override
    public RecipeSerializer<?> getType() {
	    return RecipeManager.getRecipeSerializer(getTypeId());
    }

    @Nullable
    @Override
    public JsonObject serializeAdvancement() {
        return null;
    }

    @Nullable
    @Override
    public ResourceLocation getAdvancementId() {
        return null;
    }

    @Override
    public void save(Consumer<FinishedRecipe> consumer) {
        consumer.accept(this);
    }

}
