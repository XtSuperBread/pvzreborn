package com.xstudio.pvzreborn.data.custom;

import net.minecraft.data.recipes.FinishedRecipe;

import java.util.function.Consumer;

public interface ISaveable {
    void save(Consumer<FinishedRecipe> consumer);
}
