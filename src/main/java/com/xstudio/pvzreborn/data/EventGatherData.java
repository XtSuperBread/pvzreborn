package com.xstudio.pvzreborn.data;

import com.xstudio.pvzreborn.data.provider.PVZBlockStateProvider;
import com.xstudio.pvzreborn.data.provider.PVZBlockTagsProvider;
import com.xstudio.pvzreborn.data.provider.PVZCraftingTableRecipeProvider;
import com.xstudio.pvzreborn.data.provider.PVZGlobalLootModifierProvider;
import com.xstudio.pvzreborn.data.provider.PVZItemModelProvider;
import com.xstudio.pvzreborn.data.provider.PVZLootTableProvider;
import com.xstudio.pvzreborn.data.provider.PVZPCTRecipeProvider;
import com.xstudio.pvzreborn.data.provider.PVZSPCRecipeProvider;
import com.xstudio.pvzreborn.data.provider.PVZSoundProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;

@EventBusSubscriber(bus = Bus.MOD)
public class EventGatherData {

    @SubscribeEvent
    public static void dataGen(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();

        generator.addProvider(new PVZSPCRecipeProvider(generator));
        generator.addProvider(new PVZLootTableProvider(generator));
        generator.addProvider(new PVZPCTRecipeProvider(generator));
        generator.addProvider(new PVZGlobalLootModifierProvider(generator));
        generator.addProvider(new PVZCraftingTableRecipeProvider(generator));
        generator.addProvider(new PVZSoundProvider(generator, existingFileHelper));
        generator.addProvider(new PVZItemModelProvider(generator, existingFileHelper));
        generator.addProvider(new PVZBlockStateProvider(generator, existingFileHelper));
        generator.addProvider(new PVZBlockTagsProvider(generator, existingFileHelper));
    }

}
