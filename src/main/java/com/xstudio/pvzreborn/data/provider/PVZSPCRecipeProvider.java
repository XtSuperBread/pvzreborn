package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.data.custom.ICustomRecipeProvider;
import com.xstudio.pvzreborn.data.custom.SPCFinishedRecipe;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.world.item.Items;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class PVZSPCRecipeProvider extends PVZRecipeProvider implements ICustomRecipeProvider {

    public PVZSPCRecipeProvider(DataGenerator pGenerator) {
        super(pGenerator);
    }

    @Override
    protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {
		SPCFinishedRecipe.Builder.of(3)
				.output(ItemManager.getItem(ItemNames.WHITE_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.WHITE_POLAR_DYE))
                .addInput(Items.WHEAT_SEEDS)
                .addInput(Items.BONE_MEAL)
                .requireEnergy(400)
                .tick(160)
                .build(consumer);

		SPCFinishedRecipe.Builder.of(4)
				.output(ItemManager.getItem(ItemNames.GREEN_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.GREEN_POLAR_DYE))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
                .addInput(Items.CACTUS)
                .addInput(Items.BEETROOT_SEEDS)
                .requireEnergy(800)
                .tick(160)
                .build(consumer);

		SPCFinishedRecipe.Builder.of(4)
				.output(ItemManager.getItem(ItemNames.BLUE_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.BLUE_POLAR_DYE))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
                .addInput(Items.CARROT)
                .addInput(Items.BLUE_TERRACOTTA)
                .requireEnergy(1600)
                .tick(160)
                .build(consumer);

		SPCFinishedRecipe.Builder.of(4)
				.output(ItemManager.getItem(ItemNames.PURPLE_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.PURPLE_POLAR_DYE))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
                .addInput(Items.POTATO)
                .addInput(Items.PURPLE_DYE)
                .requireEnergy(3200)
                .tick(160)
                .build(consumer);

		SPCFinishedRecipe.Builder.of(4)
				.output(ItemManager.getItem(ItemNames.GOLDEN_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.GOLDEN_POLAR_DYE))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
                .addInput(Items.PUMPKIN_SEEDS)
                .addInput(Items.GOLD_INGOT)
                .requireEnergy(4800)
                .tick(160)
                .build(consumer);

		SPCFinishedRecipe.Builder.of(4)
				.output(ItemManager.getItem(ItemNames.RED_SEED_PACKET))
				.addInput(ItemManager.getItem(ItemNames.RED_POLAR_DYE))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
                .addInput(Items.MELON_SLICE)
                .addInput(Items.COCOA_BEANS)
                .requireEnergy(6400)
                .tick(160)
                .build(consumer);
    }

    @Override
    public String getFolderName() {
        return BlockNames.SEED_PACKET_CREATOR;
    }

    @NotNull
    @Override
    public String getName() {
        return "Seed Packet Creator Recipes";
    }

}
