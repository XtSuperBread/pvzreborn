package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.block.BlockManager;
import net.minecraft.data.DataGenerator;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.VariantBlockStateBuilder;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="https://gitee.com/Bread_NiceCat">Bread_NiceCat</a>
 * @date 2023/1/1 17:54
 */
public class PVZBlockStateProvider extends BlockStateProvider {
	public static Set<Block> horizontal = new HashSet<>();
	private final ExistingFileHelper existingFileHelper;

	public PVZBlockStateProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, PVZReborn.MOD_ID, existingFileHelper);
		this.existingFileHelper = existingFileHelper;
	}

	@Override
	protected void registerStatesAndModels() {
		horizontal.forEach((block) -> horizontalBlock(block, new ModelFile.ExistingModelFile(blockTexture(block), existingFileHelper)));
		BlockManager.getBlockStateModel("crop").forEach(this::createCropBlockModel);
		BlockManager.getBlockStateModel("tree").forEach(this::createTreeBlockModel);
	}

	private void createTreeBlockModel(String blockName) {
		String name = "block/" + blockName;
		getVariantBuilder(BlockManager.getBlock(blockName))
				.partialState()
				.modelForState()
				.modelFile(models().cross(name, modLoc(name)))
				.addModel();
	}

	private void createCropBlockModel(String blockName) {
		CropBlock block = (CropBlock) BlockManager.getBlock(blockName);
		VariantBlockStateBuilder builder = getVariantBuilder(block);
		for (int i = 0; i <= block.getMaxAge(); i++) {
			String name = "block/" + blockName + "_stage_" + i;
			builder.partialState()
					.with(block.getAgeProperty(), i)
					.modelForState()
					.modelFile(models().crop(name, modLoc(name)))
					.addModel();
		}
	}

}
