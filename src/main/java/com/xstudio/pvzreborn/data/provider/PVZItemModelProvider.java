package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.item.ItemManager;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

/**
 * @author Bread_NiceCat
 */
public class PVZItemModelProvider extends ItemModelProvider {

    public PVZItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, PVZReborn.MOD_ID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
	    ItemManager.getTextureGroups().forEach((group, itemNames) -> {
				    if (group.equals(".hide")) {
					    return;
				    }
				    itemNames.forEach(name ->
							singleTexture(
									name,
									new ResourceLocation("item/generated"),
									"layer0",
									PVZReborn.prefix("item/" + group + "/" + name)));
			    }
        );
    }

}
