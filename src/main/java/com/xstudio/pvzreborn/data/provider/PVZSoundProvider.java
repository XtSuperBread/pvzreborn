package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.PVZReborn;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.SoundDefinitionsProvider;

public class PVZSoundProvider extends SoundDefinitionsProvider {

	public PVZSoundProvider(DataGenerator generator, ExistingFileHelper helper) {
		super(generator, PVZReborn.MOD_ID, helper);
	}

	@Override
	public void registerSounds() {
		ResourceLocation rl = PVZReborn.prefix("soul");
		add(rl, definition().with(sound(rl).stream()).replace(true));
	}

}
