package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class PVZCraftingTableRecipeProvider extends PVZRecipeProvider {

	public PVZCraftingTableRecipeProvider(DataGenerator generator) {
		super(generator);
	}

	private static InventoryChangeTrigger.TriggerInstance has(ItemLike pItemLike) {
		return inventoryTrigger(ItemPredicate.Builder.item().of(pItemLike).build());
	}

	private static InventoryChangeTrigger.TriggerInstance inventoryTrigger(ItemPredicate... pPredicates) {
		return new InventoryChangeTrigger.TriggerInstance(EntityPredicate.Composite.ANY, MinMaxBounds.Ints.ANY, MinMaxBounds.Ints.ANY, MinMaxBounds.Ints.ANY, pPredicates);
	}

	@Override
	protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {
		ShapelessRecipeBuilder.shapeless(ItemManager.getItem(ItemNames.ANCIENT_EYE))
				.requires(Items.GLASS_BOTTLE)
				.requires(Items.ENDER_EYE)
				.requires(Items.BLAZE_POWDER)
				.requires(Items.DRAGON_BREATH)
				.requires(Items.CHORUS_FRUIT)
				.requires(Items.TWISTING_VINES)
				.unlockedBy("has_ender_eye", has(Items.ENDER_EYE))
				.save(consumer);

		ShapelessRecipeBuilder.shapeless(ItemManager.getItem(ItemNames.REDSTONE_ARM))
				.requires(Items.PISTON)
				.requires(Items.PISTON)
				.requires(Items.SLIME_BLOCK)
				.requires(Items.ARMOR_STAND)
				.requires(Items.STICK)
				.requires(Items.IRON_BLOCK)
				.unlockedBy("has_piston", has(Items.PISTON))
				.save(consumer);

		ShapelessRecipeBuilder.shapeless(ItemManager.getItem(ItemNames.SHARP_ENDER_CRYSTAL_SHARD), 32)
				.requires(Items.END_CRYSTAL, 8)
				.requires(Items.DIAMOND)
				.unlockedBy("has_end_crystal", has(Items.END_CRYSTAL))
				.save(consumer);

		ShapelessRecipeBuilder.shapeless(ItemManager.getItem(ItemNames.HUGE_PEA), 4)
				.requires(Items.CAULDRON)
				.requires(Items.GRASS)
				.requires(Items.SLIME_BLOCK)
				.requires(Items.DISPENSER)
				.unlockedBy("has_glass", has(Items.GRASS))
				.save(consumer);

		ShapedRecipeBuilder.shaped(ItemManager.getItem(ItemNames.EMPTY_TEST_TUBE), 5)
				.pattern("G G")
				.pattern("G G")
				.pattern(" G ")
				.define('G', Items.GLASS_PANE)
				.unlockedBy("has_glass_pane", has(Items.GLASS_PANE))
				.save(consumer);

		ShapedRecipeBuilder.shaped(ItemManager.getItem(ItemNames.WARPED_WART_TEST_TUBE), 8)
				.pattern("EEE")
				.pattern("ENE")
				.pattern("EEE")
				.define('E', ItemManager.getItem(ItemNames.EMPTY_TEST_TUBE))
				.define('N', Items.WARPED_WART_BLOCK)
				.unlockedBy("has_empty_test_tube", has(ItemManager.getItem(ItemNames.EMPTY_TEST_TUBE)))
				.save(consumer);

        ShapedRecipeBuilder.shaped(ItemManager.getItem(BlockNames.POLAR_CRAFTING_TABLE))
				.pattern("BDB")
				.pattern(" C ")
				.pattern("ADE")
				.define('B', Items.GLASS)
				.define('A', Items.IRON_BLOCK)
				.define('C', Items.CRAFTING_TABLE)
				.define('D', Items.SMOOTH_STONE)
				.define('E', Items.DAYLIGHT_DETECTOR)
				.unlockedBy("has_daylight_detector", has(Items.DAYLIGHT_DETECTOR))
				.save(consumer);
	}

	@NotNull
	@Override
	public String getName() {
		return "Recipes";
	}

}
