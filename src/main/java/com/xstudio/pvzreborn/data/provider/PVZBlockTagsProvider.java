package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.block.BlockManager;
import com.xstudio.pvzreborn.block.BlockNames;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Set;

public class PVZBlockTagsProvider extends BlockTagsProvider {

    public static Set<Block> mineableByPickaxe = new HashSet<>();

    public PVZBlockTagsProvider(DataGenerator pGenerator, @Nullable ExistingFileHelper existingFileHelper) {
        super(pGenerator, PVZReborn.MOD_ID, existingFileHelper);
    }

    @Override
    protected void addTags() {
        tag(BlockTags.MINEABLE_WITH_PICKAXE).add(mineableByPickaxe.toArray(new Block[0]));
        tag(BlockTags.CROPS).add(BlockManager.getBlock(BlockNames.PEA_SEEDLINGS));
    }
}
