package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.PVZReborn;
import com.xstudio.pvzreborn.loot.LootModifierManager;
import com.xstudio.pvzreborn.loot.TorchLootModifier;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.GlobalLootModifierProvider;
import net.minecraftforge.common.loot.IGlobalLootModifier;

public class PVZGlobalLootModifierProvider extends GlobalLootModifierProvider {

    public PVZGlobalLootModifierProvider(DataGenerator gen) {
        super(gen, PVZReborn.MOD_ID);
    }

    @Override
    protected void start() {
        add(TorchLootModifier.NAME, new TorchLootModifier());
    }

    private <T extends IGlobalLootModifier> void add(String name, T instance) {
	    add(LootModifierManager.prefixName(name), LootModifierManager.getSerializer(LootModifierManager.prefixName(name)), instance);
    }

}
