package com.xstudio.pvzreborn.data.provider;

import com.mojang.datafixers.util.Pair;
import com.xstudio.pvzreborn.loot.PVZBlockLoot;
import com.xstudio.pvzreborn.loot.PVZChestLoot;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTables;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSet;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class PVZLootTableProvider extends LootTableProvider {

    public PVZLootTableProvider(DataGenerator pGenerator) {
        super(pGenerator);
    }

    protected List<Pair<Supplier<Consumer<BiConsumer<ResourceLocation, LootTable.Builder>>>, LootContextParamSet>> getTables() {
        return List.of(
            Pair.of(PVZChestLoot::new, LootContextParamSets.CHEST),
            Pair.of(PVZBlockLoot::new, LootContextParamSets.BLOCK)
        );
    }

    protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext context) {
        map.forEach((key, value) -> LootTables.validate(context, key, value));
    }

}
