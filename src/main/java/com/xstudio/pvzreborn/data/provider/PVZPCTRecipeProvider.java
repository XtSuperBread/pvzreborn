package com.xstudio.pvzreborn.data.provider;

import com.xstudio.pvzreborn.block.BlockNames;
import com.xstudio.pvzreborn.data.custom.ICustomRecipeProvider;
import com.xstudio.pvzreborn.data.custom.PCTFinishedRecipe;
import com.xstudio.pvzreborn.item.ItemManager;
import com.xstudio.pvzreborn.item.ItemNames;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class PVZPCTRecipeProvider extends PVZRecipeProvider implements ICustomRecipeProvider {

	public PVZPCTRecipeProvider(DataGenerator generator) {
		super(generator);
	}

	private static PCTFinishedRecipe.Builder testTubeBuilder(String kind, String coreItemName) {
		Item warpedWart = ItemManager.getItem(ItemNames.WARPED_WART_TEST_TUBE);
		return PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(kind), 8)
				.pattern("WWW")
				.pattern("WCW")
				.pattern("WWW")
				.define('W', warpedWart)
				.define('C', ItemManager.getItem(coreItemName))
				.requireEnergy(500)
				.tick(200);
	}

	@Override
	public String getFolderName() {
        return BlockNames.POLAR_CRAFTING_TABLE;
	}

	@NotNull
	@Override
	public String getName() {
		return "Polar Crafting Table Recipes";
	}

	@Override
	protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.WHITE_POLAR_DYE), 16)
				.shapeless()
				.addInput(Items.BONE)
				.addInput(Items.ROTTEN_FLESH)
				.addInput(Items.IRON_INGOT)
				.tick(160)
				.requireEnergy(400)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.GREEN_POLAR_DYE), 16)
				.shapeless()
				.addInput(Items.GRASS)
				.addInput(Items.JUNGLE_LEAVES)
				.tick(160)
				.requireEnergy(800)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.BLUE_POLAR_DYE), 16)
				.shapeless()
				.addInput(Items.WATER_BUCKET)
				.addInput(Items.BLUE_DYE)
				.tick(160)
				.requireEnergy(1600)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PURPLE_POLAR_DYE), 16)
				.shapeless()
				.addInput(Items.END_CRYSTAL)
				.addInput(Items.PURPLE_DYE)
				.tick(160)
				.requireEnergy(3200)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.GOLDEN_POLAR_DYE), 16)
				.shapeless()
				.addInput(Items.GOLDEN_APPLE)
				.addInput(Items.COPPER_BLOCK)
				.tick(160)
				.requireEnergy(4800)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.RED_POLAR_DYE), 16)
				.shapeless()
				.addInput(ItemManager.getItem(ItemNames.RUBY))
				.addInput(Items.GLISTERING_MELON_SLICE)
				.addInput(Items.POPPY)
				.tick(160)
				.requireEnergy(6400)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.RUBY))
				.shapeless()
				.addInput(Items.DIAMOND)
				.addInput(Items.REDSTONE)
				.addInput(Items.DIORITE)
				.tick(320)
				.requireEnergy(500)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY), 2)
				.shapeless()
				.addInput(ItemManager.getItem(ItemNames.ENTITY_LIGHT))
				.addInput(Items.IRON_INGOT)
				.addInput(Items.GLISTERING_MELON_SLICE)
				.addInput(Items.GOLDEN_CARROT)
				.addInput(Items.DAYLIGHT_DETECTOR)
				.addInput(Items.LANTERN)
				.addInput(Items.NETHERITE_INGOT)
				.requireEnergy(2000)
				.tick(600)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.NATURAL_DIAMOND))
				.shapeless()
				.addInput(Items.DIAMOND)
				.addInput(Items.COCOA_BEANS)
				.addInput(Items.BEETROOT)
				.addInput(Items.MELON)
				.addInput(Items.PUMPKIN)
				.addInput(ItemManager.getItem(ItemNames.NUT))
				.addInput(ItemManager.getItem(ItemNames.STARFRUIT))
				.addInput(ItemManager.getItem(ItemNames.SQUASH))
				.addInput(ItemManager.getItem(ItemNames.WISDOM_TREE_ESSENCE))
				.requireEnergy(1000)
				.tick(600)
				.build(consumer);
        PCTFinishedRecipe.builder()
                .output(ItemManager.getItem(BlockNames.SEED_PACKET_CREATOR))
				.pattern("  B")
				.pattern("ACA")
				.pattern("DDE")
				.define('B', Items.GLASS)
				.define('A', Items.IRON_BLOCK)
				.define('C', Items.CRAFTING_TABLE)
				.define('D', Items.SMOOTH_STONE)
				.define('E', Items.DAYLIGHT_DETECTOR)
				.requireEnergy(1000)
				.tick(100)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.KASUALIX_POTATO))
				.pattern("PPP")
				.pattern("PUP")
				.pattern("PPP")
				.define('P', Items.POTATO)
				.define('U', ItemManager.getItem(ItemNames.UNCONTROLLABLE_NETHER_STAR))
				.requireEnergy(8192)
				.tick(1)
				.build(consumer);
		testTubeBuilder(ItemNames.MAGIC_TEST_TUBE, ItemNames.UNCONTROLLABLE_NETHER_STAR)
				.build(consumer);
		testTubeBuilder(ItemNames.ICE_TEST_TUBE, ItemNames.EIGHTEENTH_STATE_ICE)
				.build(consumer);
		testTubeBuilder(ItemNames.THUNDER_TEST_TUBE, ItemNames.OVERLOADED_REDSTONE)
				.build(consumer);
		testTubeBuilder(ItemNames.EXPLOSION_TEST_TUBE, ItemNames.CREEPER_GLAND)
				.build(consumer);
		testTubeBuilder(ItemNames.AID_TEST_TUBE, ItemNames.IRON_GOLEM_SCULPTURE_SHARD)
				.build(consumer);
		testTubeBuilder(ItemNames.WARRIOR_TEST_TUBE, ItemNames.DARK_KNIGHT_SWORD)
				.build(consumer);
		testTubeBuilder(ItemNames.POISON_TEST_TUBE, ItemNames.COMPACT_THALLIUM_SULFATE)
				.build(consumer);
		testTubeBuilder(ItemNames.SHARPNESS_TEST_TUBE, ItemNames.SHARP_ENDER_CRYSTAL_SHARD)
				.build(consumer);
		testTubeBuilder(ItemNames.PROPELLANT_TEST_TUBE, ItemNames.HUGE_PEA)
				.build(consumer);
		testTubeBuilder(ItemNames.CONTROL_TEST_TUBE, ItemNames.ANCIENT_EYE)
				.build(consumer);
		testTubeBuilder(ItemNames.FIRE_TEST_TUBE, ItemNames.SUPERCRITICAL_FIRE)
				.build(consumer);
		testTubeBuilder(ItemNames.MECHANICAL_TEST_TUBE, ItemNames.REDSTONE_ARM)
				.build(consumer);
		testTubeBuilder(ItemNames.SUNSHINE_TEST_TUBE, ItemNames.ENTITY_LIGHT)
				.build(consumer);
		testTubeBuilder(ItemNames.DARKNESS_TEST_TUBE, ItemNames.BEDROCK_DUST)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.NATURAL_DIAMOND_HELMET))
				.pattern("TNT")
				.pattern("NDN")
				.pattern("WGW")
				.define('T', Ingredient.of(ItemTags.SAPLINGS))
				.define('N', ItemManager.getItem(ItemNames.NATURAL_DIAMOND))
				.define('D', Items.DIAMOND_HELMET)
				.define('W', Items.VINE)
				.define('G', Items.GOLDEN_APPLE)
				.requireEnergy(1500)
				.tick(200)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.NATURAL_DIAMOND_CHESTPLATE))
				.pattern("CNC")
				.pattern("NDN")
				.pattern("NAN")
				.define('C', Items.COPPER_ORE)
				.define('N', ItemManager.getItem(ItemNames.NATURAL_DIAMOND))
				.define('D', Items.DIAMOND_CHESTPLATE)
				.define('A', Items.AMETHYST_BLOCK)
				.requireEnergy(1500)
				.tick(200)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.NATURAL_DIAMOND_LEGGINGS))
				.pattern("GNG")
				.pattern("SDS")
				.pattern("NMN")
				.define('G', Items.GRASS)
				.define('N', ItemManager.getItem(ItemNames.NATURAL_DIAMOND))
				.define('D', Items.DIAMOND_LEGGINGS)
				.define('S', Items.COBWEB)
				.define('M', Items.RED_MUSHROOM)
				.requireEnergy(1500)
				.tick(200)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.NATURAL_DIAMOND_BOOTS))
				.pattern("TBT")
				.pattern("NDN")
				.pattern("NBN")
				.define('N', ItemManager.getItem(ItemNames.NATURAL_DIAMOND))
				.define('T', Items.LIGHT_BLUE_GLAZED_TERRACOTTA)
				.define('D', Items.DIAMOND_BOOTS)
				.define('B', Items.LIGHT_BLUE_DYE)
				.requireEnergy(1500)
				.tick(200)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY_HELMET))
				.pattern("SPS")
				.pattern("PDP")
				.pattern(" L ")
				.define('S', Items.DAYLIGHT_DETECTOR)
				.define('P', ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY))
				.define('D', ItemManager.getItem(ItemNames.NATURAL_DIAMOND_HELMET))
				.define('L', Items.CARVED_PUMPKIN)
				.requireEnergy(2500)
				.tick(300)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY_CHESTPLATE))
				.pattern("OPO")
				.pattern("PDP")
				.pattern("PLP")
				.define('O', Items.CRYING_OBSIDIAN)
				.define('P', ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY))
				.define('D', ItemManager.getItem(ItemNames.NATURAL_DIAMOND_CHESTPLATE))
				.define('L', Items.ANCIENT_DEBRIS)
				.requireEnergy(2500)
				.tick(300)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY_LEGGINGS))
				.pattern("OPO")
				.pattern("PDP")
				.pattern("NPN")
				.define('O', Items.CHISELED_POLISHED_BLACKSTONE)
				.define('P', ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY))
				.define('D', ItemManager.getItem(ItemNames.NATURAL_DIAMOND_LEGGINGS))
				.define('N', Items.NETHER_BRICK)
				.requireEnergy(2500)
				.tick(300)
				.build(consumer);
		PCTFinishedRecipe.builder()
				.output(ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY_BOOTS))
				.pattern("BRB")
				.pattern("PDP")
				.pattern("POP")
				.define('O', Items.CRYING_OBSIDIAN)
				.define('P', ItemManager.getItem(ItemNames.PHOTOVOLTAIC_ALLOY))
				.define('D', ItemManager.getItem(ItemNames.NATURAL_DIAMOND_BOOTS))
				.define('B', Items.CHISELED_POLISHED_BLACKSTONE)
				.define('R', Items.GILDED_BLACKSTONE)
				.requireEnergy(2500)
				.tick(300)
				.build(consumer);
	}

}
