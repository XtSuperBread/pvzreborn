package com.xstudio.pvzreborn.network;

import com.xstudio.pvzreborn.capability.DaveCoin;
import com.xstudio.pvzreborn.utils.NetworkUtils;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.NetworkEvent.Context;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public class DaveCoinPacket extends PVZPacketBase {

	private final int daveCoin;

	public DaveCoinPacket(@NotNull LazyOptional<DaveCoin> daveCoin) {
		this.daveCoin = daveCoin.isPresent() ? daveCoin.resolve().get().getValue() : 0;
	}

	public DaveCoinPacket(@NotNull FriendlyByteBuf buf) {
		this.daveCoin = buf.readVarInt();
	}

	@Override
	public boolean canSend() {
		return daveCoin >= 0;
	}

	@Override
	public void toBytes(@NotNull FriendlyByteBuf buf) {
		buf.writeVarInt(daveCoin);
	}

	@Override
	public void handler(@NotNull Supplier<Context> supplier) {
		supplier.get().enqueueWork(() -> NetworkUtils.syncDaveCoinData(daveCoin));
		super.handler(supplier);
	}

}
