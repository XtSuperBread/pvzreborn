package com.xstudio.pvzreborn.network;

public interface CanSend {
    boolean canSend();
}
