package com.xstudio.pvzreborn.network;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent.Context;

import java.util.function.Supplier;

public abstract class PVZPacketBase implements CanSend {

    public PVZPacketBase() {
    }

    public PVZPacketBase(FriendlyByteBuf buf) {
    }

    public abstract void toBytes(FriendlyByteBuf buf);

    public void handler(Supplier<Context> supplier) {
        supplier.get().setPacketHandled(true);
    }

}
