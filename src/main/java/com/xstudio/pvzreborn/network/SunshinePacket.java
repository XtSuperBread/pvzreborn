package com.xstudio.pvzreborn.network;

import com.xstudio.pvzreborn.capability.Sunshine;
import com.xstudio.pvzreborn.utils.NetworkUtils;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.NetworkEvent.Context;

import java.util.function.Supplier;

public class SunshinePacket extends PVZPacketBase {

	private final int sunshine;

	public SunshinePacket(LazyOptional<Sunshine> sunshine) {
		this.sunshine = sunshine.isPresent() ? sunshine.resolve().get().getValue() : 0;
	}

	public SunshinePacket(FriendlyByteBuf buf) {
		this.sunshine = buf.readVarInt();
	}

	@Override
	public boolean canSend() {
		return sunshine >= 0;
	}

	@Override
	public void toBytes(FriendlyByteBuf buf) {
		buf.writeVarInt(sunshine);
	}

	@Override
	public void handler(Supplier<Context> supplier) {
		supplier.get().enqueueWork(() -> NetworkUtils.syncSunshineData(sunshine));
		super.handler(supplier);
	}

}
