package com.xstudio.pvzreborn.network;

import com.xstudio.pvzreborn.capability.PlantFood;
import com.xstudio.pvzreborn.utils.NetworkUtils;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.NetworkEvent.Context;

import java.util.function.Supplier;

public class PlantFoodPacket extends PVZPacketBase {

	private final int plantFood;

	public PlantFoodPacket(LazyOptional<PlantFood> plantFood) {
		this.plantFood = plantFood.isPresent() ? plantFood.resolve().get().getValue() : 0;
	}

	public PlantFoodPacket(FriendlyByteBuf buf) {
		this.plantFood = buf.readVarInt();
	}

	@Override
	public boolean canSend() {
		return plantFood >= 0;
	}

	@Override
	public void toBytes(FriendlyByteBuf buf) {
		buf.writeVarInt(plantFood);
	}

	@Override
	public void handler(Supplier<Context> supplier) {
		supplier.get().enqueueWork(() -> NetworkUtils.syncPlantFoodData(plantFood));
		super.handler(supplier);
	}

}
