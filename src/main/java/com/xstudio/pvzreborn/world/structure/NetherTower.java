package com.xstudio.pvzreborn.world.structure;

import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.structure.PoolElementStructurePiece;
import net.minecraft.world.level.levelgen.structure.pools.JigsawPlacement;
import org.jetbrains.annotations.NotNull;

public class NetherTower extends StructureFeatureBase {

    public NetherTower() {
        super(context -> JigsawPlacement.addPieces(
            context,
            PoolElementStructurePiece::new,
            context.chunkPos().getMiddleBlockPosition(-10),
            false,
            true
        ));
    }

    @NotNull
    @Override
    public GenerationStep.Decoration step() {
        return Decoration.UNDERGROUND_STRUCTURES;
    }

}
