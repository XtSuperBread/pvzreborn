package com.xstudio.pvzreborn.world.structure;

import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.feature.StructureFeature;
import net.minecraft.world.level.levelgen.feature.configurations.JigsawConfiguration;
import net.minecraft.world.level.levelgen.structure.PoolElementStructurePiece;
import net.minecraft.world.level.levelgen.structure.pieces.PieceGeneratorSupplier;
import net.minecraft.world.level.levelgen.structure.pools.JigsawPlacement;
import org.jetbrains.annotations.NotNull;

public class StructureFeatureBase extends StructureFeature<JigsawConfiguration> {

    public StructureFeatureBase() {
        super(JigsawConfiguration.CODEC, context -> JigsawPlacement.addPieces(
            context,
            PoolElementStructurePiece::new,
            context.chunkPos().getMiddleBlockPosition(0),
            false,
            true
        ));
    }

    public StructureFeatureBase(PieceGeneratorSupplier<JigsawConfiguration> pieceGenerator) {
        super(JigsawConfiguration.CODEC, pieceGenerator);
    }

    @NotNull
    @Override
    public GenerationStep.Decoration step() {
        return Decoration.VEGETAL_DECORATION;
    }

}
