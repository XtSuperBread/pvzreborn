package com.xstudio.pvzreborn.recipe;

/**
 * @author Bread_NiceCat
 */
public interface IHasShape {
	boolean isShapeless(RecipeBase recipe);
}
