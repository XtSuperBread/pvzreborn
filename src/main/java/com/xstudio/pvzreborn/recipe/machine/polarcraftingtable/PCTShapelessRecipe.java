package com.xstudio.pvzreborn.recipe.machine.polarcraftingtable;

import com.xstudio.pvzreborn.recipe.RecipeBase;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.List;

public class PCTShapelessRecipe extends PCTRecipeBase {

	public PCTShapelessRecipe(ResourceLocation id, ItemStack output, List<Ingredient> inputs, int requireEnergy, int tick) {
		super(id, output, inputs, requireEnergy, tick);
	}

	@Override
	public boolean matches(List<ItemStack> actualStacks, int haveEnergy) {
		if (haveEnergy >= requireEnergy && inputs.size() == actualStacks.size()) {
			out:
			for (Ingredient input : inputs) {
				for (ItemStack actualStack : actualStacks) {
					if (input.test(actualStack)) {
						continue out;
					}
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isShapeless(RecipeBase recipe) {
		return true;
	}
}
