package com.xstudio.pvzreborn.recipe.machine;

import com.xstudio.pvzreborn.recipe.IHasShape;
import com.xstudio.pvzreborn.recipe.RecipeBase;
import net.minecraft.resources.ResourceLocation;

public abstract class MachineRecipeBase extends RecipeBase implements IHasShape {
	protected MachineRecipeBase(ResourceLocation id) {
		super(id);
	}

	public abstract int getTick();

	public abstract int getRequireEnergy();
}
