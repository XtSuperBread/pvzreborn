package com.xstudio.pvzreborn.recipe.machine.polarcraftingtable;

import com.xstudio.pvzreborn.recipe.RecipeBase;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.List;

public class PCTShapedRecipe extends PCTRecipeBase {

	public PCTShapedRecipe(ResourceLocation id, ItemStack output, List<Ingredient> inputs, int requireEnergy, int tick) {
		super(id, output, inputs, requireEnergy, tick);
	}

	@Override
	public boolean matches(List<ItemStack> actualStacks, int haveEnergy) {
		if (haveEnergy >= requireEnergy && inputs.size() == actualStacks.size()) {
			for (int i = 0; i < inputs.size(); i++) {
				if (!inputs.get(i).test(actualStacks.get(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isShapeless(RecipeBase recipe) {
		return false;
	}
}
