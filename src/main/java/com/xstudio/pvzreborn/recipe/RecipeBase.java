package com.xstudio.pvzreborn.recipe;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

public abstract class RecipeBase implements Recipe<Container> {

    protected final ResourceLocation id;

    protected RecipeBase(ResourceLocation id) {
        this.id = id;
    }

    public abstract String getTypeId();

    @Override
    @NotNull
    public RecipeSerializer<?> getSerializer() {
        return RecipeManager.getRecipeSerializer(getTypeId());
    }

    @Override
    public RecipeType<?> getType() {
	    return RecipeManager.getRecipeType(getTypeId());
    }

    @Override
    public boolean matches(Container pContainer, Level pLevel) {
        return false;
    }

    @Override
    public ItemStack assemble(Container pContainer) {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return false;
    }

    @Override
    public boolean isSpecial() {
        return true;
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

}
