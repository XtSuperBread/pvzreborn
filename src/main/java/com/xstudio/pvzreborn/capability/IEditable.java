package com.xstudio.pvzreborn.capability;

import com.xstudio.pvzreborn.network.ManagerNetwork;
import com.xstudio.pvzreborn.utils.CapabilityUtils;
import com.xstudio.pvzreborn.utils.NetworkUtils;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * @author Bread_NiceCat
 */
public interface IEditable<T extends Tag> extends INBTSerializable<T> {

	default void syncTo(ServerPlayer player) {
		ManagerNetwork.sendToPlayer(NetworkUtils.createPacketByName(getName(), CapabilityUtils.getEditorByName(getName(), player)), player);
	}

	String getName();

	int getValue();

	/**
	 * @return 返回变更后的值
	 */
	int setValue(int value);

	/**
	 * 自动同步
	 *
	 * @return 返回变更后的值
	 */
	default int setValue(int value, ServerPlayer player) {
		int i = setValue(value);
		syncTo(player);
		return i;
	}

	/**
	 * @return 返回变更后的值
	 */
	default int add(int delta) {
		return setValue(getValue() + delta);
	}

	/**
	 * 自动同步
	 *
	 * @return 返回变更后的值
	 */
	default int add(int delta, ServerPlayer player) {
		int i = add(delta);
		syncTo(player);
		return i;
	}

	/**
	 * @return 返回变更后的值
	 */
	default int remove(int delta) {
		return add(-delta);
	}

	/**
	 * 自动同步
	 *
	 * @return 返回变更后的值
	 */
	default int remove(int delta, ServerPlayer player) {
		int i = remove(delta);
		syncTo(player);
		return i;
	}

}
