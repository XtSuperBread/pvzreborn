package com.xstudio.pvzreborn.capability;

import net.minecraft.nbt.IntTag;

public class DaveCoin implements IEditable<IntTag> {

	public static final String NAME = "davecoin";
	private int coinAmount = 0;

	@Override
	public int getValue() {
		return coinAmount;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int setValue(int coinAmount) {
		if (coinAmount < 0) {
			coinAmount = 0;
		}
		this.coinAmount = coinAmount;
		return getValue();
	}

	@Override
	public IntTag serializeNBT() {
		return IntTag.valueOf(coinAmount);
	}

	@Override
	public void deserializeNBT(IntTag nbt) {
		coinAmount = nbt.getAsInt();
	}

}
