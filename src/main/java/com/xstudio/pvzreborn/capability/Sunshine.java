package com.xstudio.pvzreborn.capability;

import net.minecraft.nbt.FloatTag;
import net.minecraft.server.level.ServerPlayer;

/**
 * @author superhelo
 */
public class Sunshine implements IEditable<FloatTag> {

	public static final String NAME = "sunshine";
	private final int max;
	private float energy = 0;//储存能量用float 读取返回int

	public Sunshine(int max) {
		this.max = max;
	}

	public int getMaxValue() {
		return max;
	}

	@Override
	public int getValue() {
		return (int) energy;
	}

	public float add(float delta) {
		return setValue(energy + delta);
	}

	public boolean isFull() {
		return this.energy >= this.max;
	}

	@Override
	public FloatTag serializeNBT() {
		return FloatTag.valueOf(energy);
	}

	@Override
	public void deserializeNBT(FloatTag nbt) {
		setValue(nbt.getAsFloat());
	}

	@Override
	public String getName() {
		return NAME;
	}

	public float add(float delta, ServerPlayer player) {
		add(delta);
		syncTo(player);
		return this.energy;
	}

	@Override
	public int setValue(int energy) {
		setValue((float) energy);
		return getValue();
	}

	public float setValue(float energy) {
		if (energy > max) {
			this.energy = max;
		} else if (energy < 0) {
			this.energy = 0;
		} else {
			this.energy = energy;
		}
		return this.energy;
	}

}
