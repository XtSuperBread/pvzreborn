package com.xstudio.pvzreborn.capability;

import net.minecraft.core.Direction;
import net.minecraft.nbt.Tag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BaseCapProvider<C extends INBTSerializable<T>, T extends Tag> implements ICapabilitySerializable<T> {

	protected final C instance;
	protected final LazyOptional<C> lazyOptional;
	private final Capability<C> capability;

	public BaseCapProvider(C instance, Capability<C> capability) {
		this.instance = instance;
		this.capability = capability;
		this.lazyOptional = LazyOptional.of(() -> instance);
	}

	@NotNull
	@Override
	public <R> LazyOptional<R> getCapability(@NotNull Capability<R> cap, @Nullable Direction side) {
		return cap.equals(capability) ? lazyOptional.cast() : LazyOptional.empty();
	}

	@Override
	public T serializeNBT() {
		return instance.serializeNBT();
	}

	@Override
	public void deserializeNBT(T nbt) {
		instance.deserializeNBT(nbt);
	}

}
