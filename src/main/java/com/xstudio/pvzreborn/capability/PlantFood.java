package com.xstudio.pvzreborn.capability;

import net.minecraft.nbt.IntTag;

public class PlantFood implements IEditable<IntTag> {

	public static final String NAME = "plantfood";
	private final int max;
	private int amount = 0;

	public PlantFood(int max) {
		this.max = max;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int getValue() {
		return amount;
	}

	@Override
	public int setValue(int amount) {
		if (amount < 0) {
			amount = 0;
		} else if (amount > max) {
			amount = max;
		}
		this.amount = amount;
		return getValue();
	}

	public boolean isFull() {
		return this.amount >= this.max;
	}

	@Override
	public IntTag serializeNBT() {
		return IntTag.valueOf(amount);
	}

	@Override
	public void deserializeNBT(IntTag nbt) {
		amount = nbt.getAsInt();
	}

	public int getMaxValue() {
		return max;
	}

}
